if nc -z localhost 9200 && nc -z localhost 15672; then
	echo "dockers are up"
else
	echo "dockers are down and will be started now"
	sudo sysctl -w vm.max_map_count=262144
	echo "max map count set to 262144"
	sudo docker-compose up
	echo "dockers are started"
fi

exit 0
