package eu.hradio.commons;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.*;
import java.util.Date;


/***
 * Provides common utils
 */
public class CommonUtils
{
    /**
     * converts String into Date
     * @param dateString String containing date
     * @param formatPattern pattern to format String
     * @return Date object or null if an exception occured
     */
    public static Date toDate(String dateString, String formatPattern)
    {
        Date date = null;

        try
        {
            date = new SimpleDateFormat(formatPattern).parse(dateString);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    /**
     * converts String to DateTime object
     * @param dateString String containing date
     * @param formatPattern pattern to format String
     * @return DateTime object
     */
    public static DateTime toDateTime(String dateString, String formatPattern) {

        DateTime dateTime = null;

        try
        {
            DateTimeFormatter dtf = DateTimeFormat.forPattern(formatPattern);

            // Parsing the date
            dateTime = dtf.parseDateTime(dateString);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return dateTime;
    }


    /**
     * determines whether interval contains a date
     * @param start start date of interval
     * @param end end date of interval
     * @param when date to check if in between
     * @return true if interval [start-end] contains when, otherwise false
     */
    public static boolean isBetween(Date start, Date end, Date when) {
        return start.before(when) && end.after(when) || start.equals(when) || end.equals(when);
    }

    /**
     * determines whether interval contains a date
     * @param start start date of interval
     * @param end end date of interval
     * @param when date to check if in between
     * @return true if interval [start-end] contains when, otherwise false
     */
    public static boolean isBetween(DateTime start, DateTime end, DateTime when) {
        return start.isBefore(when) && end.isAfter(when) || start.equals(when) || end.equals(when);
    }

    /**
     * checks if String ONLY contains special characters
     * @param value String to check
     * @return true is String does not ONLY contain special characters, otherwise false
     */
    public static boolean onlyConsistsOfSpecialChars(String value) {
        String specialChars = "-/@#$%^&_+=()\n\t " ;
        return value.matches("[" + specialChars + "]+");
    }


    /**
     * checks if an URL is accessible/exists
     *
     * @param url URL to be checked
     * @return true if accessible, otherwise false
     */
    public static boolean isAccessible(String url)
    {
        boolean accessible = false;

        try
        {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("HEAD");
            int responseCode = connection.getResponseCode();
            accessible = responseCode == 200;
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return accessible;
    }


    public static byte[] toByteArray(Object obj)
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] output = null;

        try
        {
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            out.flush();
            output = bos.toByteArray();
            bos.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return output;
    }

    public static void logoToConsole()
    {
        System.out.println(" _    _ _____                _____ _____ ____  ");
        System.out.println("| |  | |  __ \\      /\\    |  __ \\_ _/  __ \\ ");
        System.out.println("| |__| | |__) |     /  \\   | |  | || || |  | |");
        System.out.println("|  __  |  _  /     / /\\ \\ | |  | || || |  | |");
        System.out.println("| |  | | | \\ \\  / ____  \\| |__| || || |__| |");
        System.out.println("|_|  |_|_|  \\_\\/_/    \\_\\_____/_____\\____/ ");
    }
}
