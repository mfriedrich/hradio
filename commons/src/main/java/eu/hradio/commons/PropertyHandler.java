package eu.hradio.commons;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/***
 * Reads the config file {application.properties} and creates RMQConfig
 */
public class PropertyHandler {

    public static final String TAG = "hradio.commons.: ";

    /**
     * Create and return RabbitMQ Config from local properties file
     * */
    public static RMQConfig getRMQConfig(String propFileName){

        Properties mProps = parseProperties(propFileName);
        RMQConfig mConfig = null;

        if (mProps != null) {

            String address = mProps.getProperty("rabbitmq.address");
            String username = mProps.getProperty("rabbitmq.user");
            String password = mProps.getProperty("rabbitmq.password");
            int port = Integer.parseInt(mProps.getProperty("rabbitmq.port"));
            int retries = Integer.parseInt(mProps.getProperty("rabbitmq.retries"));
            int retryInterval = Integer.parseInt(mProps.getProperty("rabbitmq.retryInterval"));

            mConfig = new RMQConfig(address, username, password, port, retries, retryInterval);

            //TODO integrate logging and handle misconfiguration
        }

        return mConfig;
    }

    /**
     * Create and return RESTConfig object from local properties file
     * */
    /*public static ESConfig getRESTConfig(String propFileName){

        Properties mProps = parseProperties(propFileName);
        ESConfig mConfig = null;

        if (mProps != null) {

            String protocol = mProps.getProperty("rest.protocol");
            String address = mProps.getProperty("rest.address");
            int port = Integer.parseInt(mProps.getProperty("rest.port"));
            int timeout = Integer.parseInt(mProps.getProperty("rest.requestTimeout"));

            mConfig = new ESConfig(protocol, address, port, timeout);

            //TODO integrate logging and handle misconfiguration
        }

        return mConfig;
    }*/


    /**
     * Read properties fild from input stream and return properties object for further use
     * */
    public static Properties parseProperties(String propFileName){

        Properties prop = null;
        InputStream inputStream = null;

        try {
            prop = new Properties();
            inputStream = PropertyHandler.class.getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
                System.out.println(TAG + "Property file was loaded successfully.");
            } else {
                throw new FileNotFoundException(TAG + "file: '" + propFileName + "' not found in the classpath");
            }

            // TODO Integrate Logging

        } catch (Exception e) {
            System.out.println(TAG + "Config file loading was aborted with the following exception: " + e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return prop;
    }
}
