package eu.hradio.commons;

import java.util.Objects;


/***
 * Describes a pair of two objects
 * @param <S> object of type S
 * @param <T> object of type T
 */
public class Pair<S,T> {

    public final S x;
    public final T y;

    public Pair(final S x, final T y)
    {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pair)) {
            return false;
        }

        final Pair pair = (Pair) o;

        if (x != pair.x) {
            return false;
        }
        if (y != pair.y) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(x, y);
    }
}