package eu.hradio.commons;

//TODO: Better encapsulation of RabbitMQ API and interface extraction.

import com.rabbitmq.client.*;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


/***
 * Creates and closes connection to RabbitMQ server
 */
public class RMQConnection
{
    static org.apache.logging.log4j.Logger _log = LogManager.getLogger(RMQConnection.class);


    public RMQConnection(RMQConfig rmqConfig) throws IOException, TimeoutException, InterruptedException
    {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rmqConfig.getAddress());
        factory.setPassword(rmqConfig.getPassword());
        factory.setUsername(rmqConfig.getUsername());
        factory.setPort(rmqConfig.getPort());

        int curRetry = 1;
        boolean isConnected = false;
        do
        {
            try
            {
                _log.info("Try to connect to RabbitMQ server (" + curRetry + "|" + rmqConfig.getRetries() + ").");

                _connection = factory.newConnection();
                isConnected = true;
            } catch (Exception ex)
            {
                curRetry++;
                Thread.sleep(rmqConfig.getRetryInterval());
            }
        }
        while(!isConnected && curRetry <= rmqConfig.getRetries());

        if(!isConnected)
            throw new IOException("Could not connect to RabbitMQ Broker.");
    }

    public void close() throws IOException
    {
        if(_connection.isOpen())
            _connection.close();
    }

    public Channel createChannel() throws IOException
    {
        return _connection.createChannel();
    }

    private Connection _connection;
}
