package eu.hradio.commons;


/***
 * Defines structure of the RabbitMQ configuration
 */
public class RMQConfig
{
    public RMQConfig(String _address, String _username, String _password, int _port, int retries, int retryInterval)
    {
        this._address = _address;
        this._username = _username;
        this._password = _password;
        this._port = _port;
        this._retries = retries;
        this._retryInterval = retryInterval;
    }

    public String getAddress() {
        return _address;
    }

    public String getUsername() {
        return _username;
    }

    public String getPassword() {
        return _password;
    }

    public int getPort() {
        return _port;
    }

    public int getRetries()
    {
        return _retries;
    }

    public int getRetryInterval()
    {
        return _retryInterval;
    }

    private String _address;
    private String _username;
    private String _password;
    private int _port;

    private int _retries;
    private int _retryInterval;
}

