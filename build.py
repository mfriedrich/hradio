from subprocess import Popen, PIPE
import os
from glob import glob
import stat
import shutil
import json

version = "0.1.0"
configReplacements = [["rabbitmq.address=localhost", "rabbitmq.address=rabbit"],
                      ["elastic.address=localhost", "elastic.address=elasticsearch"]]
sourceDir = os.path.dirname(os.path.realpath(__file__))
outputDir = sourceDir + "/build"


def do(arg):
    print("Do: " + str(arg))
    process = Popen(arg, stdout=PIPE)
    (output, err) = process.communicate()
    exitCode = process.wait()
    print("Result: " + " SUCCESS" if exitCode is 0 else " ERROR" + "(" + str(exitCode) + ")")
    return exitCode


def cd(dir):
    print("Goto: " + dir)
    os.chdir(dir)


def rmdir(dir):
    shutil.rmtree(dir, ignore_errors=True)


def mkdir(dir):
    os.makedirs(dir)


def cp(src, dst):
    shutil.copy(src, dst)


def componentPath(componentDir, component):
    return componentDir + "/" + component


def inJarFile(inputDir, component, version):
    versionStr = "-" + version if len(version) > 0 else ""
    return componentPath(inputDir, component) + "/build/libs/" + component + versionStr + ".jar"


def outJarDir(outputDir, component):
    return componentPath(outputDir, component) + "/build/libs/"


def outJarFile(outputDir, component, version):
    versionStr = "-" + version if len(version) > 0 else ""
    return outJarDir(outputDir, component) + component + versionStr + ".jar"


def mkComponentFolder(dir, component):
    mkdir(componentPath(dir, component))


def mkLogFolder(dir, component):
    mkdir(componentPath(dir, component) + "/log")


def copyJar(inputDir, outputDir, component, version):
    outJar = outJarFile(outputDir, component, version)
    mkdir(outJarDir(outputDir, component))
    cp(inJarFile(inputDir, component, version), outJar)


def copyComponentAppConfig(inputDir, outputDir, component):
    inPath = componentPath(inputDir, component) + "/build/resources/"
    outPath = componentPath(outputDir, component) + "/build/resources/"
    shutil.copytree(inPath, outPath)


def replace_in_config_json(outputDir, component, config_file):
    path = componentPath(outputDir, component)

    file = open(path + "/" + config_file)
    config_json = json.load(file)
    file.close()

    # replace rabbit mq and elastic host address
    if 'RabbitMQ' in config_json:
        rmq = config_json['RabbitMQ']
        if 'Address' in rmq:
            rmq['Address'] = 'rabbit'

    if 'ElasticSearch' in config_json:
        elastic = config_json['ElasticSearch']
        if 'Address' in elastic:
            elastic['Address'] = 'elastic'

    with open(path + "/config.json", 'w') as output:
        json.dump(config_json, output)


def replaceInComponentAppConfigFiles(outputDir, component, replacements):
    path = componentPath(outputDir, component) + "/build/resources/"

    files = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.properties'))]

    for file in files:

        lines = []
        with open(file, "r") as f:
            for line in f:
                lines.append(line)

        with open(file, "w") as f:
            for line in lines:
                modLine = line
                for replacement in replacements:
                    modLine = modLine.replace(replacement[0], replacement[1])

                f.write(modLine)


def copyComponentFile(inputDir, outputDir, component, file):
    cp(componentPath(inputDir, component) + "/" + file, componentPath(outputDir, component) + "/" + file)


def copyDockerfile(inputDir, outputDir, component):
    cp(componentPath(inputDir, component) + "/Dockerfile", componentPath(outputDir, component) + "/Dockerfile")


def writeStartScript(outputDir):
    fileName = outputDir + "/start.sh"
    f = open(fileName, "w+")
    f.write("sudo sysctl -w vm.max_map_count=262144\n")
    f.write("cd docker\n")
    f.write("sudo docker-compose up\n")
    f.close()

    st = os.stat(fileName)
    os.chmod(fileName, st.st_mode | stat.S_IEXEC)


if __name__ == "__main__":
    # Build Java components

    cd(sourceDir + "/commons")
    do(["gradle", "build"])

    cd(sourceDir + "/metadata")
    do(["gradle", "build"])

    cd(sourceDir + "/metadata_processor")
    do(["gradle", "build"])

    cd(sourceDir + "/search_processor")
    do(["gradle", "build"])

    cd(sourceDir + "/metadata_rest")
    do(["gradle", "build"])

    cd(sourceDir + "/search_rest")
    do(["gradle", "build"])

    # Copy all components
    do(["mkdir", outputDir])
    cd(outputDir)
    cd("..")

    mkComponentFolder(outputDir, "commons")
    copyJar(sourceDir, outputDir, "commons", version)

    mkComponentFolder(outputDir, "metadata")
    copyJar(sourceDir, outputDir, "metadata", version)

    mkComponentFolder(outputDir, "metadata_processor")
    copyJar(sourceDir, outputDir, "metadata_processor", version)
    copyDockerfile(sourceDir, outputDir, "metadata_processor")
    copyComponentAppConfig(sourceDir, outputDir, "metadata_processor")
    replaceInComponentAppConfigFiles(outputDir, "metadata_processor", configReplacements)

    mkComponentFolder(outputDir, "metadata_rest")
    copyJar(sourceDir, outputDir, "metadata_rest", version)
    copyDockerfile(sourceDir, outputDir, "metadata_rest")
    copyComponentAppConfig(sourceDir, outputDir, "metadata_rest")
    replaceInComponentAppConfigFiles(outputDir, "metadata_rest", configReplacements)

    mkComponentFolder(outputDir, "search_processor")
    copyJar(sourceDir, outputDir, "search_processor", version)
    copyDockerfile(sourceDir, outputDir, "search_processor")
    copyComponentAppConfig(sourceDir, outputDir, "search_processor")
    replaceInComponentAppConfigFiles(outputDir, "search_processor", configReplacements)

    mkComponentFolder(outputDir, "search_rest")
    copyJar(sourceDir, outputDir, "search_rest", version)
    copyDockerfile(sourceDir, outputDir, "search_rest")
    copyComponentAppConfig(sourceDir, outputDir, "search_rest")
    replaceInComponentAppConfigFiles(outputDir, "search_rest", configReplacements)

    mkComponentFolder(outputDir, "radiodns_importer")
    copyComponentFile(sourceDir, outputDir, "radiodns_importer", "radiodns_importer.py")
    copyComponentFile(sourceDir, outputDir, "radiodns_importer", "config.json")
    copyDockerfile(sourceDir, outputDir, "radiodns_importer")
    replace_in_config_json(outputDir, "radiodns_importer", "config.json")

    mkComponentFolder(outputDir, "dl_importer")
    copyComponentFile(sourceDir, outputDir, "dl_importer", "dl_importer.py")
    copyComponentFile(sourceDir, outputDir, "dl_importer", "config.json")
    copyDockerfile(sourceDir, outputDir, "dl_importer")
    replace_in_config_json(outputDir, "dl_importer", "config.json")

    # Copy Docker Compose config
    mkComponentFolder(outputDir, "docker")
    cp(sourceDir + "/docker/docker-compose.yml", outputDir + "/docker/docker-compose.yml")

    # Create Docker images.
    cd(outputDir + "/docker")
    do(["sudo", "docker-compose", "build"])

    # Write startup script.
    writeStartScript(outputDir)
