import unittest
from test_scripts.helper_utils import *
from dl_importer.dl_importer import *

rest_url = "http://localhost:8090/api/v1"

dns_test_data = json.load(open("test_data_dns.json"))
dl_test_data = json.load(open("test_data_dl.json"))
dns_dl_mapping = json.load(open("dns_dl_test_data_mapping.json"))


class TestDlImporter(unittest.TestCase):

    def setUp(self):
        self.channel = None
        self.rmq_queue = None
        self.connection = None

    def test1_metadata_put(self):
        """
        publishes rest_data via metadata_rest service
        """
        print("Connect to " + rest_url + " ...")
        requests.get(rest_url)
        print("The URL exists")

        for data_set in dns_test_data:
            rest_data = unpack_data(data_set, accepted_keys=["serviceprovider", "service", "programme", "schedule"])
            mapping = data_set["target_mapping"]

            self.assertNotEqual(None, mapping)

            rest_data = json.dumps(rest_data)

            filled_in_data = fill_placeholders([mapping, rest_data])
            mapping = filled_in_data[0]
            rest_data = filled_in_data[1]

            url = rest_url + mapping

            print("Publishing: " + rest_data)
            print("to " + url)
            r = requests.put(url, rest_data)

            self.assertEqual(201, r.status_code)

        # run dl importer
        os.system("python ../dl_importer/dl_importer.py \"dl_importer_config.json\"")

        for data_set in dns_dl_mapping:
            mapping = fill_placeholders([data_set["target_mapping"]])[0]
            service_id = data_set["dl_service_id"]

            url_mapping = rest_url + mapping

            print("Searching programme event for service " + str(service_id) + " here: " + url_mapping)

            d = check_for_item_status(200, url_mapping, 5, 1)

            print("Found: " + str(d))
            self.assertTrue(len(d["programmeEvents"]) > 0)

    def tearDown(self):
        # Clean test data from ES
        mappings = set()

        for dns in dns_test_data:
            mapping = dns["target_mapping"]
            dns_id = unpack_data(dns, accepted_keys=["serviceprovider", "service", "programme", "schedule"])["id"]
            mapping += "/" + dns_id
            mapping = fill_placeholders([mapping])[0]
            mappings.add(mapping)

        mappings = list(mappings)
        mappings.sort(key=lambda s: len(s), reverse=False)

        # delete service provider
        requests.delete(rest_url + mappings[0])

        # check if the service provider and all its children were deleted
        for m in mappings:
            r = requests.get(rest_url + m)
            self.assertEqual(404, r.status_code)
            print("Mapping " + m + " was deleted successfully.")


unittest.main()
