# Helper utils fot test modules
from datetime import datetime, timedelta, timezone
import json
import re
import requests
import time

mapping_sp = "/providers"
mapping_service = "/services"
mapping_sched = "/schedules"
mapping_prog = "/programmes"
es_datetime_format = "%b %d, %Y %H:%M:%S"
es_timezone = 'US/Eastern'


def to_hex(dec):
    x = (dec % 16)
    digits = "0123456789ABCDEF"
    rest = dec / 16
    if rest == 0:
        return digits[x]
    return to_hex(rest) + digits[x]


def get_key(json_data, accepted_keys=None):
    if accepted_keys is not None:
        accepted_keys = [acc_key.lower() for acc_key in accepted_keys]
        for key in json_data.keys():
            if key.lower() in accepted_keys:
                return key
        else:
            return None
    return list(json_data.keys())[0]


def create_mapping(service_provider_id=None, service_id=None, schedule_date=None, programme_id=None):
    if service_provider_id is None:
        return mapping_sp

    if service_id is None:
        return mapping_sp + '/' + service_provider_id

    if schedule_date is None:
        return mapping_sp + '/' + service_provider_id + mapping_service + '/' + service_id

    if programme_id is None:
        return mapping_sp + '/' + service_provider_id + mapping_service + '/' + service_id + \
               mapping_sched + '/' + schedule_date

    return mapping_sp + '/' + service_provider_id + mapping_service + '/' + service_id + mapping_sched + '/' + \
           schedule_date + mapping_prog + '/' + programme_id


def unpack_data(json_data, accepted_keys=None):
    data_type = get_key(json_data, accepted_keys)
    return json_data[data_type]


def fill_placeholders(data_list):
    processed_data = []

    today = datetime.now(timezone.utc).strftime("%d%m%Y")
    now_tomorrow = (datetime.now(timezone.utc) + timedelta(days=1)).strftime(es_datetime_format)
    now = datetime.now(timezone.utc).strftime(es_datetime_format)
    now_before_two_hours = (datetime.now(timezone.utc) - timedelta(hours=2)).strftime(es_datetime_format)
    now_in_two_hours = (datetime.now(timezone.utc) + timedelta(hours=2)).strftime(es_datetime_format)
    now_in_four_hours = (datetime.now(timezone.utc) + timedelta(hours=4)).strftime(es_datetime_format)

    for elem in data_list:
        d = elem.replace("%TODAY%", today).replace("%NOW%", now) \
            .replace("%NOW_BEFORE_TWO_HOURS%", now_before_two_hours).replace("%NOW_IN_TWO_HOURS%", now_in_two_hours) \
            .replace("%NOW_TOMORROW%", now_tomorrow).replace("%NOW_IN_FOUR_HOURS%", now_in_four_hours)

        r_list = re.findall("%ID_.*?%", d)

        for r in r_list:
            str_id = r.replace("%ID_", "").replace("%", "")
            d = d.replace(r, hash_code(str_id))

        processed_data.append(d)

    return processed_data


def check_for_item_status(status, url, tries, sleep):
    ret = requests.get(url)
    check_tries = 1

    while check_tries < tries and ret.status_code != status:
        time.sleep(sleep)
        check_tries = check_tries + 1
        ret = requests.get(url)

    if ret.status_code == 200:
        return json.loads(ret.content)

    return {}


def hash_code(s):
    h = 0
    for c in s:
        h = (31 * h + ord(c)) & 0xFFFFFFFF
    return str(((h + 0x80000000) & 0xFFFFFFFF) - 0x80000000)
