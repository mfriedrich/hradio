import unittest
from test_scripts.helper_utils import *

rest_address = "localhost"
rest_port = 8090
rest_request_timeout = 3000
rest_api = "/api/v1"
rest_url = "http://" + rest_address + ":" + str(rest_port) + rest_api


class PutGetDeleteTests(unittest.TestCase):

    def test_provider(self):
        d = {"name": "ServiceProviderTest1", "description": "A test service provider", "id": "0"}
        r = requests.put(rest_url + "/providers", json.dumps(d))
        self.assertEqual(r.status_code, 201)

        d = check_for_item_status(200, rest_url + "/providers/0", 5, 1)
        self.assertEqual(d["name"], "ServiceProviderTest1")
        self.assertEqual(d["description"], "A test service provider")
        self.assertEqual(d["id"], "0")

        r = requests.delete(rest_url + "/providers/0")
        check_for_item_status(404, rest_url + "/providers/0", 5, 1)

        r = requests.delete(rest_url + "/providers/0")
        self.assertEqual(r.status_code, 404)

    def test_service(self):
        d = {"name": "ServiceProviderTest1", "description": "A test service provider", "id": "0"}
        r = requests.put(rest_url + "/providers", json.dumps(d))
        self.assertEqual(r.status_code, 201)

        d = {"name": "ServiceTest1", "description": "A test service", "id": "0"}
        r = requests.put(rest_url + "/providers/0/services", json.dumps(d))
        self.assertEqual(r.status_code, 201)

        r = requests.get(rest_url + "/providers/0/services/0")
        d = check_for_item_status(200, rest_url + "/providers/0/services/0", 5, 1)
        self.assertEqual(d["name"], "ServiceTest1")
        self.assertEqual(d["description"], "A test service")
        self.assertEqual(d["id"], "0")
        self.assertEqual(d["serviceProviderId"], "0")

        r = requests.delete(rest_url + "/providers/0/services/0")
        check_for_item_status(404, rest_url + "/providers/0/services/0", 5, 1)

        r = requests.delete(rest_url + "/providers/0")
        check_for_item_status(404, rest_url + "/providers/0", 5, 1)

    def test_service_delete_sp(self):
        d = {"name": "ServiceProviderTest1", "description": "A test service provider", "id": "0"}
        r = requests.put(rest_url + "/providers", json.dumps(d))
        self.assertEqual(r.status_code, 201)

        d = {"name": "ServiceTest1", "description": "A test service", "id": "0"}
        r = requests.put(rest_url + "/providers/0/services", json.dumps(d))
        self.assertEqual(r.status_code, 201)

        d = check_for_item_status(200, rest_url + "/providers/0/services/0", 5, 1)
        self.assertEqual(d["name"], "ServiceTest1")
        self.assertEqual(d["description"], "A test service")
        self.assertEqual(d["id"], "0")
        self.assertEqual(d["serviceProviderId"], "0")

        r = requests.delete(rest_url + "/providers/0")

        check_for_item_status(404, rest_url + "/providers/0/services/0", 5, 1)

        check_for_item_status(404, rest_url + "/providers/0", 5, 1)

    def test_schedule_programme(self):
        d = {"name": "ServiceProviderTest1", "description": "A test service provider", "id": "0"}
        r = requests.put(rest_url + "/providers", json.dumps(d))
        self.assertEqual(r.status_code, 201)

        d = {"name": "ServiceTest1", "description": "A test service", "id": "0"}
        r = requests.put(rest_url + "/providers/0/services", json.dumps(d))
        self.assertEqual(r.status_code, 201)

        d = {"startTime": "2018-04-12T00:00:00+02:00", "stopTime": "2018-04-13T00:00:00+02:00", "scopes": [{"name": "scope1"}, {"name": "scope2"}], "id": "0"}
        r = requests.put(rest_url + "/providers/0/services/0/schedules", json.dumps(d))
        self.assertEqual(r.status_code, 201)

        d = {"name": "Programme 1", "id": "0",
             "description": "A test programme",
             "startTime": "2018-04-12T00:00:00+02:00",
             "stopTime": "2018-04-12T01:00:00+02:00",
             "genres": [{"name": "Pop"}, {"name": "Rock"}],
             "bearers": [{"type": "http", "address": "www.google.de"}, {"type": "fm", "address": "123"}]
             }
        r = requests.put(rest_url + "/providers/0/services/0/schedules/0/programmes", json.dumps(d))
        self.assertEqual(r.status_code, 201)

        d = check_for_item_status(200, rest_url + "/providers/0/services/0/schedules/0/programmes/0", 5, 1)
      
        r = requests.delete(rest_url + "/providers/0/services/0/schedules/0/programmes/0")
        self.assertEqual(r.status_code, 200)

        check_for_item_status(404,
                                       rest_url + "/providers/0/services/0/schedules/0/programmes/0",
                                       5, 1)

        r = requests.delete(rest_url + "/providers/0/services/0/schedules/0")
        self.assertEqual(r.status_code, 200)

        check_for_item_status(404,
                                       rest_url + "/providers/0/services/0/schedules/0",
                                       5, 1)


if __name__ == '__main__':
    unittest.main()
