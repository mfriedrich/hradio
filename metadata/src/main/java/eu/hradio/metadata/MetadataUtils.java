package eu.hradio.metadata;

import com.google.gson.*;
import eu.hradio.metadata.search.ErrorMessages;
import eu.hradio.metadata.search.MetadataResponse;
import eu.hradio.metadata.search.MetadataResponseType;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.rest.RestStatus;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.TimeZone;


/***
 * Provides utils for metadata handling
 */
public class MetadataUtils
{
    public static final String dateFormat = "MMM dd, yyyy HH:mm:ss";
    public static final SimpleDateFormat sdf_ddMMyyyy = createSDF("ddMMyyyy", TimeZone.getTimeZone("UTC"));
    public static final SimpleDateFormat sdf_dateFormat = createSDF(dateFormat, TimeZone.getTimeZone("UTC"));

    public static final Gson gson = new GsonBuilder().setDateFormat(dateFormat).disableHtmlEscaping().create();
    public static JsonParser jsonParser = new JsonParser();

    public static String getESIndexFor(MetadataType type)
    {
        return "metadata_" + type.toString().toLowerCase();
    }

    public static String getESTypeFor(MetadataType type)
    {
        return type.toString();
    }


    public static String computeId(String content)
    {
        long hash = content.hashCode();
        //byte[] bytes = ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(hash).array();
        return Long.toString(hash); // Base64.getUrlEncoder().encodeToString(bytes);
    }


    public static MetadataType getTypeFromESType(String esType)
    {
        return MetadataType.parse(esType);
    }


    public static JsonArray transformESSearchResult(String input)
    {
        JsonArray hits = jsonParser.parse(input).getAsJsonObject().getAsJsonObject("hits").getAsJsonArray("hits");
        JsonArray newHits = new JsonArray();

        for(JsonElement hit : hits)
        {
            String type = hit.getAsJsonObject().get("_type").getAsString();
            double score = hit.getAsJsonObject().get("_score").getAsDouble();
            JsonObject service = hit.getAsJsonObject().getAsJsonObject("_source");

            JsonObject newHit = new JsonObject();
            newHit.addProperty("type", type);
            newHit.addProperty("score", score);
            newHit.add("content", service);

            newHits.add(newHit);
        }

        return newHits;
    }


    public static JsonObject transformESGetResult(String input)
    {
        JsonObject hit = jsonParser.parse(input).getAsJsonObject();

        //String type = hit.getAsJsonObject().get("_type").getAsString();
        //JsonObject service = hit.getAsJsonObject().getAsJsonObject("_source");

        //JsonObject newHit = new JsonObject();
        //newHit.addProperty("type", type);
        //newHit.add("content", service);

        return hit;//newHit;
    }


    public static String toJSONString(IMetadata metadata) {
        return gson.toJson(metadata);
    }


    /**
     * Determines the PI address using EnsembleEcc, EnsembleID and ServiceID of DL
     *
     * @param ensembleEcc decimal ensemble ecc of DL
     * @param ensembleId  decimal ensemble ID of dl
     * @param serviceId   decimal service ID from DL
     * @return URL of PI file
     */
    public static String generatePIUrl(int ensembleEcc, int ensembleId, int serviceId)
    {
        final String url = "http://epg4br.irt.de/radiodns/spi/3.1/dab/%GCC%/%EnsembleID%/%ServiceID%/0/%Date%_PI.xml";

        // Service ID must be hexadecimal value toString because for GCC we need first 4 Bits of Service ID
        String gcc = Integer.toHexString(serviceId).substring(0, 1) + Integer.toHexString(ensembleEcc);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String date = sdf.format(Date.from(Instant.now()));

        return url.replace("%GCC%", gcc)
                .replace("%EnsembleID%", Integer.toHexString(ensembleId))
                .replace("%ServiceID%", Integer.toHexString(serviceId))
                .replace("%Date%", date);
    }


    public static MetadataResponse checkMetadataResponse(SearchResponse response)
    {
        if (response.status() != RestStatus.OK)
            return new MetadataResponse(ErrorMessages.elasticSearchError, MetadataResponseType.ERROR);

        if (response.isTimedOut())
            return new MetadataResponse(ErrorMessages.elasticSearchTimeout, MetadataResponseType.TIMEOUT);

        if(response.getHits().totalHits == 0)
            return new MetadataResponse(ErrorMessages.elasticSearchNotFound, MetadataResponseType.NOT_FOUND);

        return new MetadataResponse("", MetadataResponseType.OK);
    }


    public static MetadataResponse checkMetadataResponse(DeleteResponse response)
    {
        if(response.status() == RestStatus.NOT_FOUND)
            return new MetadataResponse(ErrorMessages.elasticSearchNotFound, MetadataResponseType.NOT_FOUND);
        else if (response.status() != RestStatus.OK)
            return new MetadataResponse(ErrorMessages.elasticSearchError, MetadataResponseType.ERROR);
        else
            return new MetadataResponse("", MetadataResponseType.OK);
    }


    public static SimpleDateFormat createSDF(String dateFormat, TimeZone timezone) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(timezone);
        return sdf;
    }

}
