package eu.hradio.metadata;


/***
 * Provides constants
 */
public class Constants
{
    public static final String metadataRequestQueue = "metadata_request";
    public static final String rdnsMetadataQueue = "metadata";
    public static final String dlMetadataQueue = "dl_metadata";
    public static final String serviceQueryQueue = "service_request_query";
    public static final String scheduleQueryQueue = "schedule_request_query";
    public static final String programmeQueryQueue = "programme_request_query";


    public static final String idSeparator = "_";
}
