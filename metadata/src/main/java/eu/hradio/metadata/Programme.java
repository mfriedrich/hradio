package eu.hradio.metadata;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/***
 * Contains metadata model for programme
 */
public class Programme implements IMetadata
{
    public Programme(String id)
    {
        this.bearers = new ArrayList<>();
        this.genres = new ArrayList<>();
        this.programmeEvents = new ArrayList<>();
        this.id = id;
    }

    public void addBearer(Bearer bearer)
    {
        this.bearers.add(bearer);
    }

    public void addGenre(Genre genre)
    {
        this.genres.add(genre);
    }

    public void addProgrammeEvent(ProgrammeEvent event)
    {
        this.programmeEvents.add(event);
    }

    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name.trim();
    }

    public String getLongName()
    {
        return longName;
    }
    public void setLongName(String longName)
    {
        this.longName = longName;
    }

    public List<Bearer> getBearers()
    {
        return bearers;
    }
    public void setBearers(List<Bearer> bearers)
    {
        this.bearers = bearers;
    }

    public List<Genre> getGenres()
    {
        return genres;
    }
    public void setGenres(List<Genre> genres)
    {
        this.genres = genres;
    }

    public List<ProgrammeEvent> getProgrammeEvents()
    {
        return programmeEvents;
    }
    public void setProgrammeEvents(List<ProgrammeEvent> programmeEvents)
    {
        this.programmeEvents = programmeEvents;
    }

    public Date getStartTime()
    {
        return startTime;
    }
    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }
    public void setStartTime(DateTime startTime)
    {
        this.startTime = startTime.toDate();
    }

    public Date getStopTime()
    {
        return stopTime;
    }
    public void setStopTime(Date stopTime)
    {
        this.stopTime = stopTime;
    }
    public void setStopTime(DateTime stopTime)
    {
        this.stopTime = stopTime.toDate();
    }

    public String getServiceId() { return serviceId; }
    public void setServiceId(String serviceId) { this.serviceId = serviceId; }

    public void setScheduleId(String scheduleId) { this.scheduleId = scheduleId;}
    public String getScheduleId() { return this.scheduleId;}

    @Override
    public MetadataType getType()
    {
        return MetadataType.PROGRAMME;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public String getParentId()
    {
        return scheduleId;
    }

    @Override
    public void setId(String id)
    {
        this.id = id;
    }

    private Date startTime;
    private Date stopTime;
    private String description;
    private String name;

    private String longName;
    private String id;
    private String scheduleId;
    private String serviceId;

    private List<Bearer> bearers;
    private List<Genre> genres;
    private List<ProgrammeEvent> programmeEvents;
}
