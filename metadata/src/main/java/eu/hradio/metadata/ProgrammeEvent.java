package eu.hradio.metadata;

import org.joda.time.DateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/***
 * Contains metadata model for programme event
 */
public class ProgrammeEvent{
    private List<Content> contents;
    private Date startTime;
    private Date stopTime;

    public ProgrammeEvent() {
        this.contents = new ArrayList<>();
    }

    public ProgrammeEvent(Date start) {
        this();
        this.startTime = start;
    }

    public ProgrammeEvent(DateTime start) {
        this(start.toDate());
    }

    public void addContent(Content content) {
        this.contents.add(content);
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public void setStartTime(DateTime startTime)
    {
        this.startTime = startTime.toDate();
    }

    public List<Content> getContents()
    {
        return contents;
    }

    public void setContents(List<Content> contents)
    {
        this.contents = contents;
    }

    public Date getStopTime()
    {
        return stopTime;
    }

    public void setStopTime(Date stopTime)
    {
        this.stopTime = stopTime;
    }

    public void setStopTime(DateTime stopTime) { this.stopTime = stopTime.toDate(); }
}
