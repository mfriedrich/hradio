package eu.hradio.metadata;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static eu.hradio.commons.CommonUtils.isBetween;


/***
 * Contains metadata model for schedule
 */
public class Schedule implements IMetadata
{
    public Schedule(String id)
    {
        this.programmes = new ArrayList<>();
        this.scopes = new ArrayList<>();
        this.id = id;
    }

    public void addProgram(Programme programme)
    {
        programme.setScheduleId(this.getId());
        this.programmes.add(programme);
    }

    public void addScope(Scope scope)
    {
        this.scopes.add(scope);
    }

    public List<Scope> getScopes()
    {
        return scopes;
    }

    public List<Programme> getProgrammes()
    {
        return programmes;
    }
    public void setProgrammes(List<Programme> programmes)
    {
        this.programmes = programmes;
    }

    public Date getStopTime()
    {
        return stopTime;
    }
    public void setStopTime(Date stopTime)
    {
        this.stopTime = stopTime;
    }

    public Date getStartTime()
    {
        return startTime;
    }
    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public void setServiceId(String serviceId)
    {
        this.serviceId = serviceId;
    }
    public String getServiceId()
    {
        return this.serviceId;
    }

    public Programme getParent(ProgrammeEvent event) {

        for (Programme programme : programmes) {
            if (isBetween(programme.getStartTime(), programme.getStopTime(), event.getStartTime()) &&
                isBetween(programme.getStartTime(), programme.getStopTime(), event.getStopTime()))
                return programme;
        }

        return null;
    }

    @Override
    public MetadataType getType()
    {
        return MetadataType.SCHEDULE;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public String getParentId() { return serviceId; }

    @Override
    public void setId(String id)
    {
        this.id = id;
    }

    private Date startTime;
    private Date stopTime;
    private String serviceId;
    private String id;

    private List<Programme> programmes;
    private List<Scope> scopes;
}
