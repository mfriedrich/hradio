package eu.hradio.metadata;

//TODO save genres in xml and use de/serialization to get/add genres?


/***
 * Contains metadata model for genre
 */
public class Genre {

    //private List<String> genres;
    private String name;

    public Genre() { }//this.genres = new ArrayList<>(); }

    public Genre(String name) {
        this.name = name.trim();
    }

    public void setName(String name) { this.name = name.trim(); }

    //public List<String> getGenres() { return genres; }
}