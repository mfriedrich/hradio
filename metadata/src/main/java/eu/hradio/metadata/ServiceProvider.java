package eu.hradio.metadata;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/***
 * Contains metadata model for service provider
 */
public class ServiceProvider implements IMetadata
{
    public ServiceProvider(String id)
    {
        this.services = new ArrayList<>();
        this.id = id;
    }

    public void addService(Service service)
    {
        service.setServiceProviderId(getId());
        this.services.add(service);
    }
    public void removeService(Service service)
    {
        service.setServiceProviderId(null);
        this.services.remove(service);
    }

    public void setName(String name)
    {
        this.name = name.trim();
    }
    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public List<Service> getServices()
    {
        return services;
    }
    public void setServices(List<Service> services)
    {
        this.services = services;
    }

    @Override
    public MetadataType getType()
    {
        return MetadataType.SERVICE_PROVIDER;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public String getParentId() { return ""; }

    private String name;
    private String description;
    private List<Service> services;
    private String id;
}
