package eu.hradio.metadata;


/***
 * Interface defining the structure of metadata objects
 */
public interface IMetadata
{
    MetadataType getType();

    String getId();
    void setId(String id);

    String getParentId();
}

