package eu.hradio.metadata;


/***
 * Enum containing valid types of metadata
 */
public enum MetadataType
{
    SERVICE_PROVIDER,
    SERVICE,
    SCHEDULE,
    PROGRAMME,
    NONE;

    @Override
    public String toString() {
        switch(this) {
            case SERVICE_PROVIDER: return "ServiceProvider";
            case SERVICE: return "Service";
            case SCHEDULE: return "Schedule";
            case PROGRAMME: return "Programme";
            case NONE: return "None";
            default: throw new IllegalArgumentException();
        }
    }

    public static MetadataType parse(String input)
    {
        input = input.trim().toLowerCase();

        switch(input)
        {
            case "serviceprovider":
                return SERVICE_PROVIDER;
            case "service":
                return SERVICE;
            case "schedule":
                return SCHEDULE;
            case "programme":
                return PROGRAMME;
            default:
                return NONE;
        }
    }

    public String getParentIdPropertyName()
    {
        switch(this) {
            case SERVICE_PROVIDER: return "";
            case SERVICE: return "serviceProviderId";
            case SCHEDULE: return "serviceId";
            case PROGRAMME: return "scheduleId";
            case NONE: return "none";
            default: throw new IllegalArgumentException();
        }
    }

    public MetadataType getChildType()
    {
        switch(this) {
            case SERVICE_PROVIDER: return SERVICE;
            case SERVICE: return SCHEDULE;
            case SCHEDULE: return PROGRAMME;
            case PROGRAMME: return NONE;
            default: throw new IllegalArgumentException();
        }
    }
}

