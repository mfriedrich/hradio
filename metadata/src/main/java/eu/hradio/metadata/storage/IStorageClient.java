package eu.hradio.metadata.storage;

import eu.hradio.metadata.IMetadata;
import eu.hradio.metadata.MetadataType;
import eu.hradio.metadata.search.MetadataResponse;
import eu.hradio.metadata.search.MetadataResponseType;
import eu.hradio.metadata.search.SearchRequest;
import eu.hradio.metadata.storage.elastic.IMetadataCreator;

import java.io.IOException;
import java.util.List;


/***
 * Interface defining structure of a metadata storage class
 */
public interface IStorageClient
{
    void close() throws IOException;

    MetadataResponseType create(IMetadata metadata) throws IOException;

    IMetadata get(String id, MetadataType type) throws IOException;

    List<IMetadata> getChilds(String parentId, MetadataType type) throws IOException;

    MetadataResponseType delete(String id, MetadataType type) throws IOException;

    MetadataResponse search(SearchRequest request) throws IOException;

    IMetadataCreator getMetadataCreator(MetadataType type);
}
