package eu.hradio.metadata.storage.elastic;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.hradio.metadata.*;
import eu.hradio.metadata.search.ErrorMessages;
import eu.hradio.metadata.search.MetadataResponse;
import eu.hradio.metadata.search.MetadataResponseType;
import eu.hradio.metadata.storage.IStorageClient;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static eu.hradio.metadata.MetadataUtils.dateFormat;
import static java.util.Collections.emptySet;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;


/***
 * Provides functions for Elastic Search access:
 * store, get, create, delete ... metadata items
 */
public class ESStorageClient implements IStorageClient
{
    private static org.apache.logging.log4j.Logger _log = LogManager.getLogger(ESStorageClient.class);

    public IMetadataCreator getMetadataCreator(MetadataType type)
    {
        return _creatorMap.get(type);
    }

    //TODO
    //public ESStorageClient(ESConfig elasticConfig, IESSearchRequestBuilder requestBuilder)
    //{
    //    this._requestBuilder = requestBuilder;
    //    this(elasticConfig);
    //}

    //void setSearchBuilder(IESearchRequestBuilder builder)
    //{
    //    _requestBuilder = builder;
    //}

    public ESStorageClient(ESConfig elasticConfig, boolean createIndices) throws IOException
    {
        _esConfig = elasticConfig;

        _esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost(_esConfig.getAddress(), _esConfig.getPort(), _esConfig.getProtocol())));

        if (!testConnection())
        {
            throw new IOException("Could not connect to Elastic Search server.");
        }

        _creatorMap = new HashMap<MetadataType, IMetadataCreator>();
        _creatorMap.put(MetadataType.SERVICE_PROVIDER, new ServiceProviderCreator());
        _creatorMap.put(MetadataType.SERVICE, new ServiceCreator());
        _creatorMap.put(MetadataType.SCHEDULE, new ScheduleCreator());
        _creatorMap.put(MetadataType.PROGRAMME, new ProgrammeCreator());

        if (createIndices)
            createMetadataIndices(_esConfig.getDeleteIndicesIfExisting());
    }

    @Override
    public void close() throws IOException
    {
        _esClient.close();
    }

    @Override
    public MetadataResponseType create(IMetadata metadata) throws IOException
    {
        String json = MetadataUtils.toJSONString(metadata);
        String index = MetadataUtils.getESIndexFor(metadata.getType());

        IndexRequest indexRequest = new IndexRequest(index, metadata.getType().toString(), metadata.getId()).source(json, XContentType.JSON);

        //Important for synchronized updates.
        indexRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);

        IndexResponse resp = _esClient.index(indexRequest);

        _log.debug(String.format("Created at index %s with id %s and version %d", index, resp.getId(), resp.getVersion()));

        if(resp.status() != RestStatus.OK && resp.status() != RestStatus.CREATED)
        {
            return MetadataResponseType.ERROR;
        }
        else
        {
            return MetadataResponseType.CREATED;
        }
    }

    @Override
    public IMetadata get(String id, MetadataType type) throws IOException
    {
        IMetadataCreator creator = _creatorMap.get(type);

        String esIndex = MetadataUtils.getESIndexFor(type);
        String esType = MetadataUtils.getESTypeFor(type);

        MetadataType childType = type.getChildType();

        GetRequest getRequest = new GetRequest(esIndex, esType, id);
        GetResponse getResponse = _esClient.get(getRequest);

        if (getResponse.isSourceEmpty())
            return null;

        JsonObject obj = MetadataUtils.transformESGetResult(getResponse.getSourceAsString());
        JsonArray children = null;

        if(childType != MetadataType.NONE)
        {
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            sourceBuilder.query(QueryBuilders.termQuery(childType.getParentIdPropertyName(), id));
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.indices(MetadataUtils.getESIndexFor(childType));
            searchRequest.types(MetadataUtils.getESTypeFor(childType));
            searchRequest.source(sourceBuilder);

            SearchResponse childrenResponse = _esClient.search(searchRequest);

            MetadataResponse errorResponse = MetadataUtils.checkMetadataResponse(childrenResponse);
            if (errorResponse.getType() != MetadataResponseType.OK && errorResponse.getType() != MetadataResponseType.NOT_FOUND) //Not found is ok since it is possible that no childs are available.
                return null;

            children = MetadataUtils.transformESSearchResult(childrenResponse.toString());
        }
        else
        {
            children = new JsonArray();
        }

        return creator.prepareResult(obj, children);
    }

    @Override
    public List<IMetadata> getChilds(String parentId, MetadataType type) throws IOException
    {
        IMetadataCreator creator = _creatorMap.get(type);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        if (parentId.isEmpty())
        {
            sourceBuilder.query(QueryBuilders.matchAllQuery());
        } else
        {
            sourceBuilder.query(QueryBuilders.termQuery(type.getParentIdPropertyName(), parentId));
        }

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(MetadataUtils.getESIndexFor(type));
        searchRequest.types(MetadataUtils.getESTypeFor(type));
        searchRequest.source(sourceBuilder);

        SearchResponse response = _esClient.search(searchRequest);

        MetadataResponse errorResponse = MetadataUtils.checkMetadataResponse(response);
        if (errorResponse.getType() != MetadataResponseType.OK)
            return null;

        JsonArray objects = MetadataUtils.transformESSearchResult(response.toString());

        return creator.prepareResult(objects);
    }

    @Override
    public MetadataResponseType delete(String id, MetadataType type) throws IOException
    {
        String esIndex = MetadataUtils.getESIndexFor(type);
        String esType = MetadataUtils.getESTypeFor(type);

        if (id.isEmpty())
        {
            //We only support per-item deletes (not per item collection deletes).
            return MetadataResponseType.ERROR;
        } else
        {
            ESStorageClient.DeleteItem item = new ESStorageClient.DeleteItem(id, esIndex, esType);

            List<ESStorageClient.DeleteItem> deleteItems = new ArrayList<ESStorageClient.DeleteItem>();

            MetadataResponseType resp = fillDeleteList(deleteItems, item);
            if (resp != MetadataResponseType.OK)
                return resp;

            _log.info("Number of delete items: " + deleteItems.size());

            return deleteItemsIn(deleteItems);
        }
    }

    @Override
    public MetadataResponse search(eu.hradio.metadata.search.SearchRequest request) throws IOException
    {
        String esIndex = MetadataUtils.getESIndexFor(request.getMetadataType());

        SearchRequest esRequest = new MetadataESSearchRequestBuilder(esIndex).build(request);
        SearchResponse esResponse = _esClient.search(esRequest);

        MetadataResponse resp = checkMetadataResponse(esResponse);
        if(resp.getType() == MetadataResponseType.OK)
        {
            resp = new MetadataResponse(MetadataUtils.transformESSearchResult(esResponse.toString()).toString(), MetadataResponseType.OK);
        }

        return resp;
    }

    public RestHighLevelClient getInternalESClient()
    {
        return _esClient;
    }

    private MetadataResponse checkMetadataResponse(SearchResponse response)
    {
        if (response.status() != RestStatus.OK)
            return new MetadataResponse(ErrorMessages.elasticSearchError, MetadataResponseType.ERROR);

        if (response.isTimedOut())
            return new MetadataResponse(ErrorMessages.elasticSearchTimeout, MetadataResponseType.TIMEOUT);

        if(response.getHits().totalHits == 0)
            return new MetadataResponse(ErrorMessages.elasticSearchNotFound, MetadataResponseType.NOT_FOUND);

        return new MetadataResponse("", MetadataResponseType.OK);
    }

    private MetadataResponseType fillDeleteList(List<ESStorageClient.DeleteItem> deleteItems, ESStorageClient.DeleteItem item) throws IOException
    {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        MetadataType type = MetadataUtils.getTypeFromESType(item.getType());
        MetadataType childType = type.getChildType();

        sourceBuilder.query(QueryBuilders.termQuery(childType.getParentIdPropertyName(), item.getId()));
        SearchRequest searchRequest = new SearchRequest();

        if (childType != MetadataType.NONE)
        {
            searchRequest.indices(MetadataUtils.getESIndexFor(childType));
            searchRequest.types(MetadataUtils.getESTypeFor(childType));
        }
        searchRequest.source(sourceBuilder);

        SearchResponse response = _esClient.search(searchRequest);

        MetadataResponse errorResponse = MetadataUtils.checkMetadataResponse(response);
        if (errorResponse.getType() != MetadataResponseType.OK && errorResponse.getType() != MetadataResponseType.NOT_FOUND) //Not found is ok since it is possible that no childs are available.
            return errorResponse.getType();

        JsonArray objects = MetadataUtils.jsonParser.parse(response.toString()).getAsJsonObject().getAsJsonObject("hits").getAsJsonArray("hits");

        for(JsonElement obj : objects)
        {
            String esId =  obj.getAsJsonObject().getAsJsonPrimitive("_id").getAsString();
            String esType =  obj.getAsJsonObject().getAsJsonPrimitive("_type").getAsString();
            String esIndex =  obj.getAsJsonObject().getAsJsonPrimitive("_index").getAsString();

            fillDeleteList(deleteItems, new ESStorageClient.DeleteItem(esId, esIndex, esType));
        }

        deleteItems.add(item);

        return MetadataResponseType.OK;
    }

    private MetadataResponseType deleteItemsIn(List<ESStorageClient.DeleteItem> deleteItems) throws IOException
    {
        for(ESStorageClient.DeleteItem item : deleteItems)
        {
            DeleteRequest esRequest = new DeleteRequest(
                    item.getIndex(),
                    item.getType(),
                    item.getId());

            //Important for synchronized updates.
            esRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);

            DeleteResponse esResponse = _esClient.delete(esRequest);

            MetadataResponse errorResponse = MetadataUtils.checkMetadataResponse(esResponse);
            if (errorResponse.getType() != MetadataResponseType.OK)
                return errorResponse.getType();

            _log.info(String.format("Deleted item '%s' of type %s.", item.getId(), item.getType()));
        }

        return MetadataResponseType.OK;
    }

    private boolean testConnection() throws IOException
    {
        _log.info("Setup Elastic Search connection...");
        boolean couldNotConnect = false;
        int retries = 1;
        do
        {
            _log.info("Try to connect to Elastic Search server (" + retries + "|" + _esConfig.getRetries() + ").");

            if (retries >= _esConfig.getRetries())
            {
                couldNotConnect = true;
                break;
            }

            retries++;
            try
            {
                Thread.sleep(_esConfig.getRetryInterval());
            } catch (InterruptedException e)
            {
                return false;
            }
        }
        while (!_esConfig.ping());
        if (couldNotConnect)
        {
            return false;
        }
        _log.info("Done.");
        return true;
    }

    private void createMetadataIndices(boolean deleteIfExisting)
    {
        try
        {
            XContentBuilder providerMapping = jsonBuilder()
                    .startObject()
                    .startObject(MetadataUtils.getESTypeFor(MetadataType.SERVICE_PROVIDER))
                    .startObject("properties")
                    .startObject("id").field("type", "keyword").endObject()
                    .startObject("name").field("type", "keyword").endObject()
                    .startObject("description").field("type", "text").endObject()
                    .endObject()
                    .endObject()
                    .endObject();

            XContentBuilder serviceMapping = jsonBuilder()
                    .startObject()
                    .startObject(MetadataUtils.getESTypeFor(MetadataType.SERVICE))
                    .startObject("properties")
                    .startObject("id").field("type", "keyword").endObject()
                    .startObject("serviceProviderId").field("type", "keyword").endObject()
                    .startObject("name").field("type", "keyword").endObject()
                    .startObject("description").field("type", "text").endObject()

                    .startObject("bearers").field("type", "nested")
                    .startObject("properties")
                    .startObject("address").field("type", "keyword").endObject()
                    .startObject("type").field("type", "keyword").endObject()
                    .endObject()
                    .endObject()

                    .startObject("genres").field("type", "nested")
                    .startObject("properties")
                    .startObject("name").field("type", "text").endObject()
                    .endObject()
                    .endObject()


                    .endObject()
                    .endObject()
                    .endObject();

            XContentBuilder scheduleMapping = jsonBuilder()
                    .startObject()
                    .startObject(MetadataUtils.getESTypeFor(MetadataType.SCHEDULE))
                    .startObject("properties")
                    .startObject("id").field("type", "keyword").endObject()
                    .startObject("serviceId").field("type", "keyword").endObject()
                    .startObject("startTime").field("type", "date").field("format", dateFormat).endObject()
                    .startObject("stopTime").field("type", "date").field("format", dateFormat).endObject()

                    .startObject("scopes").field("type", "nested")
                    .startObject("properties")
                    .startObject("name").field("type", "text").endObject()
                    .endObject()
                    .endObject()

                    .endObject()
                    .endObject()
                    .endObject();

            XContentBuilder programmeMapping = jsonBuilder()
                    .startObject()
                        .startObject(MetadataUtils.getESTypeFor(MetadataType.PROGRAMME))
                            .startObject("properties")
                                .startObject("id").field("type", "keyword").endObject()
                                .startObject("scheduleId").field("type", "keyword").endObject()
                                .startObject("serviceId").field("type", "keyword").endObject()

                                .startObject("name").field("type", "keyword").endObject()
                                .startObject("description").field("type", "text").endObject()
                                .startObject("startTime").field("type", "date").field("format", dateFormat).endObject()
                                .startObject("stopTime").field("type", "date").field("format", dateFormat).endObject()

                                .startObject("programmeEvents").field("type", "nested")
                                    .startObject("properties")
                                        .startObject("id").field("type", "keyword").endObject()
                                        .startObject("startTime").field("type", "date").field("format", dateFormat).endObject()
                                        .startObject("stopTime").field("type", "date").field("format", dateFormat).endObject()

                                        .startObject("contents").field("type", "nested")
                                            .startObject("properties")
                                                .startObject("type").field("type", "text").endObject()
                                                .startObject("value").field("type", "text").endObject()
                                            .endObject()
                                        .endObject()
                                    .endObject()
                                .endObject()

                                .startObject("bearers").field("type", "nested")
                                    .startObject("properties")
                                        .startObject("address").field("type", "keyword").endObject()
                                        .startObject("type").field("type", "keyword").endObject()
                                    .endObject()
                                .endObject()

                                .startObject("genres").field("type", "nested")
                                    .startObject("properties")
                                        .startObject("name").field("type", "text").endObject()
                                    .endObject()
                                .endObject()
                            .endObject()
                        .endObject()
                    .endObject();

            createMetadataIndex(MetadataType.SERVICE_PROVIDER, deleteIfExisting, providerMapping);
            createMetadataIndex(MetadataType.SERVICE, deleteIfExisting, serviceMapping);
            createMetadataIndex(MetadataType.SCHEDULE, deleteIfExisting, scheduleMapping);
            createMetadataIndex(MetadataType.PROGRAMME, deleteIfExisting, programmeMapping);

        }
        catch (IOException e)
        {
            _log.error(e.getMessage());
        }
    }

    private void createMetadataIndex(MetadataType type, boolean deleteIfExisting, XContentBuilder mapping) throws IOException
    {
        String index = MetadataUtils.getESIndexFor(type);

        if(deleteIfExisting)
        {
            try
            {
                DeleteIndexRequest request = new DeleteIndexRequest(MetadataUtils.getESIndexFor(type));
                DeleteIndexResponse deleteIndexResponse = _esClient.indices().delete(request);
            }
            catch(ElasticsearchException ex)
            {
                _log.error(String.format("Could not delete index %s. Reason: %s", index, ex.getMessage()));
            }
        }

        try
        {
            CreateIndexRequest request = new CreateIndexRequest(MetadataUtils.getESIndexFor(type));

            if(mapping != null)
                request.mapping(MetadataUtils.getESTypeFor(type), mapping);

            CreateIndexResponse createIndexResponse = _esClient.indices().create(request);

            boolean acknowledged = createIndexResponse.isAcknowledged();

            _log.info(String.format("Index '%s' created.",MetadataUtils.getESIndexFor(type)));
        }
        catch(ElasticsearchStatusException ex)
        {
            _log.error(String.format("Could not create index %s. Reason: %s", index, ex.getMessage()));
        }
    }

    private RestHighLevelClient _esClient;
    private ESConfig _esConfig;
    private HashMap<MetadataType, IMetadataCreator> _creatorMap;

    private class DeleteItem
    {
        public String getId()
        {
            return id;
        }

        public String getIndex()
        {
            return index;
        }

        public String getType()
        {
            return type;
        }

        private String id;
        private String index;
        private String type;

        public DeleteItem(String id, String index, String type)
        {
            this.id = id;
            this.index = index;
            this.type = type;
        }
    }
}
