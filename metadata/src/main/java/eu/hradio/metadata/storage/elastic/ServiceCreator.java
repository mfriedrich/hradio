package eu.hradio.metadata.storage.elastic;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.hradio.metadata.*;
import eu.hradio.metadata.search.MetadataRequest;

import java.util.ArrayList;
import java.util.List;

/***
 * Provides utils to create an object of type Service
 */
public class ServiceCreator implements IMetadataCreator
{
    @Override
    public IMetadata createMetadataObject(Gson gson, JsonElement obj, MetadataRequest request)
    {
        Service service =  gson.fromJson(obj, Service.class);
        service.setServiceProviderId(request.getParentId());

        return service;
    }

    @Override
    public IMetadata prepareResult(JsonObject obj, JsonArray childs)
    {
        Service srv = MetadataUtils.gson.fromJson(obj, Service.class);

        for(JsonElement child : childs)
        {
            Schedule sched = MetadataUtils.gson.fromJson(child.getAsJsonObject().getAsJsonObject("content"), Schedule.class);
            srv.addSchedule(sched);
        }

        return srv;
    }

    @Override
    public List<IMetadata> prepareResult(JsonArray objects)
    {
        List<IMetadata> services = new ArrayList<IMetadata>();

        for(JsonElement obj : objects)
        {
            Service srv = MetadataUtils.gson.fromJson(obj.getAsJsonObject().getAsJsonObject("content"), Service.class);
            services.add(srv);
        }

        return services;
    }
}
