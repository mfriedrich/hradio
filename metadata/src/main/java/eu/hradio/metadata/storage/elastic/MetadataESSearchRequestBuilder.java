package eu.hradio.metadata.storage.elastic;

import eu.hradio.metadata.MetadataType;
import eu.hradio.metadata.MetadataUtils;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import eu.hradio.metadata.search.SearchRequest;

/***
 * Builds search requests
 */
public class MetadataESSearchRequestBuilder implements IESSearchRequestBuilder
{
    @Override
    public org.elasticsearch.action.search.SearchRequest build(SearchRequest request)
    {
        org.elasticsearch.action.search.SearchRequest esRequest = new org.elasticsearch.action.search.SearchRequest();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        QueryBuilder query = null;

        switch (request.getQuerySyntax())
        {
            case ELASTIC_QUERY_STRING:
                query = QueryBuilders.queryStringQuery(request.getQuery());
                break;
            case ELASTIC_SIMPLE_QUERY_STRING:
                query = QueryBuilders.simpleQueryStringQuery(request.getQuery());
                break;
            case ELASTIC_COMPLEX:
                query = QueryBuilders.wrapperQuery(request.getQuery());
                break;
        }

        searchSourceBuilder.query(query).from(request.getFrom()).size(request.getSize());

        esRequest.indices(_index);
        esRequest.types(MetadataUtils.getESTypeFor(request.getMetadataType()));

        esRequest.source(searchSourceBuilder);

        return esRequest;
    }

    public MetadataESSearchRequestBuilder(String index)
    {
        this._index = index;
    }

    private String _index;
}
