package eu.hradio.metadata.storage.elastic;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;


/***
 * Builds SearchRequest objects
 */
public class DLESSearchRequestBuilder implements IESSearchRequestBuilder
{
    @Override
    public SearchRequest build(eu.hradio.metadata.search.SearchRequest request)
    {
        SearchRequest esRequest = new org.elasticsearch.action.search.SearchRequest("dl_metadata");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        String query = request.getQuery();

        if(query.equals("*") || query.isEmpty())
        {
            searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        }
        else
        {
            searchSourceBuilder.query(QueryBuilders.matchQuery("DynamicLabel.ServiceId", query));
        }

        esRequest.source(searchSourceBuilder);

        return esRequest;
    }
}
