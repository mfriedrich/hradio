package eu.hradio.metadata.storage.elastic;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.hradio.metadata.IMetadata;
import eu.hradio.metadata.search.MetadataRequest;
import eu.hradio.metadata.MetadataType;

import java.util.List;

/***
 * Interface defining structure of a metadata creator class
 */
public interface IMetadataCreator
{
    IMetadata createMetadataObject(Gson gson, JsonElement obj, MetadataRequest request);

    IMetadata prepareResult(JsonObject obj, JsonArray childs);

    List<IMetadata> prepareResult(JsonArray objects);
}
