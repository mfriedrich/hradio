package eu.hradio.metadata.storage.elastic;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.hradio.metadata.*;
import eu.hradio.metadata.search.MetadataRequest;

import java.util.ArrayList;
import java.util.List;

import static eu.hradio.metadata.MetadataUtils.gson;


/***
 * Provides utils to create an object of type ServiceProvider
 */
public class ServiceProviderCreator implements IMetadataCreator
{
    @Override
    public IMetadata createMetadataObject(Gson gson, JsonElement obj, MetadataRequest request)
    {
        return gson.fromJson(obj, ServiceProvider.class);
    }

    @Override
    public IMetadata prepareResult(JsonObject obj, JsonArray childs)
    {
        ServiceProvider sp = gson.fromJson(obj, ServiceProvider.class);

        for(JsonElement child : childs)
        {
            Service srv = gson.fromJson(child.getAsJsonObject().getAsJsonObject("content"), Service.class);
            sp.addService(srv);
        }

        return sp;
    }

    @Override
    public List<IMetadata> prepareResult(JsonArray objects)
    {
        List<IMetadata> sps = new ArrayList<IMetadata>();

        for(JsonElement obj : objects)
        {
            ServiceProvider srv = gson.fromJson(obj.getAsJsonObject().getAsJsonObject("content"), ServiceProvider.class);
            sps.add(srv);
        }

        return sps;
    }
}
