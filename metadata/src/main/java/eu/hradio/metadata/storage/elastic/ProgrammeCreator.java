package eu.hradio.metadata.storage.elastic;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.hradio.metadata.MetadataUtils;
import eu.hradio.metadata.IMetadata;
import eu.hradio.metadata.search.MetadataRequest;
import eu.hradio.metadata.Programme;

import java.util.ArrayList;
import java.util.List;

import static eu.hradio.metadata.MetadataUtils.gson;

/***
 * Provides utils to create an object of type Programme
 */
public class ProgrammeCreator implements IMetadataCreator
{
    @Override
    public IMetadata createMetadataObject(Gson gson, JsonElement obj, MetadataRequest request)
    {
        Programme programme =  gson.fromJson(obj, Programme.class);
        programme.setScheduleId(request.getParentId());

        return programme;
    }

    @Override
    public IMetadata prepareResult(JsonObject obj, JsonArray childs)
    {
        Programme programme = gson.fromJson(obj, Programme.class);

        return programme;
    }

    @Override
    public List<IMetadata> prepareResult(JsonArray objects)
    {
        List<IMetadata> programmes = new ArrayList<IMetadata>();

        for(JsonElement obj : objects)
        {
            Programme programme = MetadataUtils.gson.fromJson(obj.getAsJsonObject().getAsJsonObject("content"), Programme.class);
            programmes.add(programme);
        }

        return programmes;
    }
}
