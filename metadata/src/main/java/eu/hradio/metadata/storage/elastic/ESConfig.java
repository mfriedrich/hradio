package eu.hradio.metadata.storage.elastic;

import org.apache.http.HttpHost;
import org.elasticsearch.action.main.MainResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;


/***
 * Defines configuration for Elastic Search server connection
 */
public class ESConfig {

    private String _address;
    private String _protocol;
    private int _port;
    private int _reqTimeout;
    private int _retries;
    private int _retryInterval;
    private boolean _deleteIndicesIfExisting;

    public ESConfig(String protocol, String address, int port, int reqTimeout)
    {

        //rest.protocol = http
        //rest.address = localhost
        //rest.port = 9200
        //rest.requestTimeout = 3000

        this._protocol = protocol;
        this._address = address;
        this._reqTimeout = reqTimeout;
        this._port = port;
    }

    public ESConfig(String protocol, String address, int port, int reqTimeout, int retries, int retryInterval, boolean deleteIndicesIfExisting)
    {
        this(protocol, address, port, reqTimeout);
        this._retries = retries;
        this._retryInterval = retryInterval;
        this._deleteIndicesIfExisting = deleteIndicesIfExisting;
    }

    public String getProtocol() { return _protocol;}

    public String getAddress() {
        return _address;
    }

    public int getRequestTimeout() {
        return _reqTimeout;
    }

    public int getPort() {
        return _port;
    }

    public int getRetries()
    {
        return _retries;
    }

    public int getRetryInterval()
    {
        return _retryInterval;
    }

    public boolean getDeleteIndicesIfExisting() { return _deleteIndicesIfExisting;}

    public boolean ping()
    {
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(new HttpHost(getAddress(), getPort(), getProtocol())));

        try
        {
            MainResponse response = client.info();
        } catch (Exception e)
        {
            return false;
        }

        return true;
    }
}

