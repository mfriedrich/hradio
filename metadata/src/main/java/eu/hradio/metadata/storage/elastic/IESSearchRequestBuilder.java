package eu.hradio.metadata.storage.elastic;


/***
 * Interface defining structure of a search request builder class
 */
public interface IESSearchRequestBuilder
{
    public org.elasticsearch.action.search.SearchRequest build(eu.hradio.metadata.search.SearchRequest request);
}
