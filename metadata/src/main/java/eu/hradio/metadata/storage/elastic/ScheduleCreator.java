package eu.hradio.metadata.storage.elastic;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.hradio.metadata.*;
import eu.hradio.metadata.search.MetadataRequest;

import java.util.ArrayList;
import java.util.List;

import static eu.hradio.metadata.MetadataUtils.gson;


/***
 * Provides utils to create an object of type Schedule
 */
public class ScheduleCreator implements IMetadataCreator
{
    @Override
    public IMetadata createMetadataObject(Gson gson, JsonElement obj, MetadataRequest request)
    {
        Schedule schedule =  gson.fromJson(obj, Schedule.class);
        schedule.setServiceId(request.getParentId());

        return schedule;
    }

    @Override
    public IMetadata prepareResult(JsonObject obj, JsonArray childs)
    {
        Schedule sched = gson.fromJson(obj, Schedule.class);

        for(JsonElement child : childs)
        {
            Programme pro = gson.fromJson(child.getAsJsonObject().getAsJsonObject("content"), Programme.class);
            sched.addProgram(pro);
        }

       return sched;
    }

    @Override
    public List<IMetadata> prepareResult(JsonArray objects)
    {
        List<IMetadata> schedules = new ArrayList<IMetadata>();

        for(JsonElement obj : objects)
        {
            Schedule schedule = gson.fromJson(obj.getAsJsonObject().getAsJsonObject("content"), Schedule.class);
            schedules.add(schedule);
        }

        return schedules;
    }
}
