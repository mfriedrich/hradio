package eu.hradio.metadata;

import javax.lang.model.type.UnknownTypeException;


/***
 * Contains metadata model for bearer
 */
public class Bearer
{
    public enum BearerType { DAB, HTTP, FM, DRM }

    private String address;
    private BearerType type;

    public Bearer(String address) {
        this.address = address;
        setType();
    }

    private void setType() {
        String adr = this.address.toLowerCase();

        if (adr.startsWith("dab")) this.type = BearerType.DAB;
        else if (adr.startsWith("http")) this.type = BearerType.HTTP;
        else if (adr.startsWith("fm")) this.type = BearerType.FM;
        else if (adr.startsWith("dm")) this.type = BearerType.DRM;

        else throw new UnknownTypeException(null, adr);
    }

    public BearerType getType() { return this.type; }

    public String getAddress() { return address; }
}
