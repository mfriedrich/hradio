package eu.hradio.metadata.search;

import eu.hradio.metadata.MetadataType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/***
 * Defines structure of a search request
 */
public class SearchRequest
{
    public SearchRequest(MetadataType metadataType, String query, int size, int from, SearchRequestQuerySyntax querySyntax, int maxSearchHops, List<String> searchHostBlacklist, List<String> searchHostWhitelist)
    {
        this.metadataType = metadataType;
        this.query = query;
        this.size = size;
        this.from = from;
        this.querySyntax = querySyntax;
        this.maxSearchHops = maxSearchHops;
        this.searchHostBlacklist = searchHostBlacklist;
        this.searchHostWhitelist = searchHostWhitelist;
        this.searchFederationMode = this.maxSearchHops == 0 ? SearchFederationMode.LOCAL : SearchFederationMode.FEDERATED;
    }

    public SearchRequest(MetadataType metadataType, String query, int size, int from, SearchRequestQuerySyntax querySyntax, int maxSearchHops, String searchHostBlacklist, String searchHostWhitelist)
    {
        this(metadataType, query, size, from, querySyntax, maxSearchHops, toStringList(searchHostBlacklist), toStringList(searchHostWhitelist));
    }

    public SearchRequest(MetadataType metadataType, String query, int size, int from, SearchRequestQuerySyntax querySyntax)
    {
        this(metadataType, query, size, from, querySyntax, defaultMaxSearchHops, new ArrayList<String>(), new ArrayList<String>());
    }

    public String getQuery()
    {
        return query;
    }

    public int getSize()
    {
        return size;
    }

    public int getFrom()
    {
        return from;
    }

    public SearchRequestQuerySyntax getQuerySyntax()
    {
        return querySyntax;
    }

    public int getMaxSearchHops()
    {
        return maxSearchHops;
    }

    public List<String> getSearchHostBlacklist()
    {
        return searchHostBlacklist;
    }

    public List<String> getSearchHostWhitelist()
    {
        return searchHostWhitelist;
    }

    public SearchFederationMode getSearchFederationMode()
    {
        return searchFederationMode;
    }

    public MetadataType getMetadataType()
    {
        return metadataType;
    }

    static List<String> toStringList(String string)
    {
        if(string.isEmpty())
            return new ArrayList<String>();
        else
            return Arrays.asList(string.split(listSeparator));

    }

    SearchRequestQuerySyntax querySyntax;
    String query;
    int size;
    int from;
    int maxSearchHops;
    List<String> searchHostBlacklist;
    List<String> searchHostWhitelist;
    private MetadataType metadataType;

    SearchFederationMode searchFederationMode;

    private static final int defaultMaxSearchHops = 0;
    private static final String listSeparator = ";";
}
