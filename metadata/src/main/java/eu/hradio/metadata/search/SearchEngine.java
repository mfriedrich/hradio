package eu.hradio.metadata.search;

/***
 * Enum containing valid search engines
 */
public enum SearchEngine
{
    DEFAULT,
    ELASTIC;

    @Override
    public String toString() {
        switch(this) {
            case DEFAULT: return "DEFAULT";
            case ELASTIC: return "ELASTIC";
            default: throw new IllegalArgumentException();
        }
    }

    public static SearchEngine parse(String string)
    {
        return  SearchEngine.valueOf(string.trim().toUpperCase());
    }
}
