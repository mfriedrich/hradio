package eu.hradio.metadata.search;

/***
 * Defines structure of a metadata response
 */
public class MetadataResponse
{
    private String content;
    private MetadataResponseType type;

    public MetadataResponse(String content, MetadataResponseType type)
    {
        this.content = content;
        this.type = type;
    }

    public String getContent()
    {
        return content;
    }

    public MetadataResponseType getType()
    {
        return type;
    }
}
