package eu.hradio.metadata.search;

import eu.hradio.commons.*;

import com.rabbitmq.client.*;
import eu.hradio.metadata.Constants;
import eu.hradio.metadata.MetadataUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

/***
 * Consumes a RabbitMQ channel: delivers metadata requests and returns the corresponding response
 */
public class MetadataRequester
{
    public class Consumer extends DefaultConsumer
    {
        public Consumer(Channel channel, HashMap<String, RequestProcessor> requests)
        {
            super(channel);

            _requests = requests;
        }

        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException
        {
            String corrId = properties.getCorrelationId();
            RequestProcessor requestProcessor = _requests.get(corrId);

            if (requestProcessor != null)
            {
                _requests.remove(corrId);

                MetadataResponse response = MetadataUtils.gson.fromJson(new String(body, "UTF-8"), MetadataResponse.class);

                requestProcessor.offerResult(response);
            }
        }

        private HashMap<String, RequestProcessor> _requests;
    }

    public MetadataRequester(RMQConnection rmqConnection, long timeout) throws IOException
    {
        this._rmqConnection = rmqConnection;
        this._timeout = timeout;

        _channel = _rmqConnection.createChannel();

        _responseQueue = _channel.queueDeclare().getQueue();

        _requestProcessors = new HashMap<String, RequestProcessor>();

        _channel.basicConsume(_responseQueue, true, new Consumer(_channel, _requestProcessors));
    }

    public MetadataResponse request(MetadataRequest request) throws IOException, InterruptedException
    {
        final String corrId = UUID.randomUUID().toString();;

        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(_responseQueue)
                .build();

        _channel.basicPublish("", _requestQueue, props, MetadataUtils.gson.toJson(request).getBytes("UTF-8"));

        RequestProcessor requestProcessor = new RequestProcessor();

        _requestProcessors.put(corrId, requestProcessor);

        MetadataResponse response = requestProcessor.waitOnResult(_timeout);
        if(response == null)
        {
            response = new MetadataResponse("", MetadataResponseType.TIMEOUT);
        }

        return response;
    }

    public void close() throws IOException, TimeoutException
    {
        if(_channel.isOpen())
            _channel.close();
    }

    private final String _requestQueue = Constants.metadataRequestQueue;
    private String _responseQueue;

    private HashMap<String, RequestProcessor> _requestProcessors;

    private long _timeout;

    private RMQConnection _rmqConnection;
    private Channel _channel;
}
