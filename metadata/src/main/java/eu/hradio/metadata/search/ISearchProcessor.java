package eu.hradio.metadata.search;

import java.io.IOException;

/***
 * Interface defining the structure of search processors
 */
public interface ISearchProcessor
{
     MetadataResponse search(eu.hradio.metadata.search.SearchRequest request) throws IOException;

}
