package eu.hradio.metadata.search;

import eu.hradio.metadata.MetadataType;

/***
 * Defines structure of a metadata request
 */
public class MetadataRequest
{
    public MetadataRequest(String jsonPayload, String itemId, String parentId, MetadataRequestType requestType, MetadataType metadataType)
    {
        this.jsonPayload = jsonPayload;
        this.itemId = itemId;
        this.parentId = parentId;
        this.requestType = requestType;
        this.metadataType = metadataType;
    }

    public String getContent()
    {
        return jsonPayload;
    }

    public String getItemId()
    {
        return itemId;
    }

    public String getParentId()
    {
        return parentId;
    }

    public MetadataRequestType getRequestType()
    {
        return requestType;
    }

    public MetadataType getMetadataType()
    {
        return metadataType;
    }

    private String jsonPayload;
    private String itemId;
    private String parentId;
    private MetadataRequestType requestType;
    private MetadataType metadataType;
}

