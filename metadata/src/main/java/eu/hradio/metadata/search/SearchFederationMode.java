package eu.hradio.metadata.search;

/***
 * Enum containing valid federation modes
 */
public enum SearchFederationMode
{
    FEDERATED,
    LOCAL;

    @Override
    public String toString() {
        switch(this) {
            case FEDERATED: return "FEDERATED";
            case LOCAL: return "LOCAL";
            default: throw new IllegalArgumentException();
        }
    }

    public static SearchFederationMode parse(String string)
    {
        return  SearchFederationMode.valueOf(string.trim().toUpperCase());
    }
}
