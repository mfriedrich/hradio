package eu.hradio.metadata.search;

/***
 * Enum containing valid types of metadata requests
 */
public enum MetadataRequestType
{
    GET,
    CREATE,
    UPDATE,
    DELETE;

    @Override
    public String toString() {
        switch(this) {
            case GET: return "Get";
            case CREATE: return "Create";
            case UPDATE: return "Update";
            case DELETE: return "Delete";
            default: throw new IllegalArgumentException();
        }
    }
}

