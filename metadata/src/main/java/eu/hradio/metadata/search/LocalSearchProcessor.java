package eu.hradio.metadata.search;

import eu.hradio.metadata.storage.IStorageClient;

import java.io.IOException;

/***
 * Defines the structure of a local search processor
 */
public class LocalSearchProcessor implements ISearchProcessor
{
    public LocalSearchProcessor(IStorageClient _client)
    {
        this._client = _client;
    }

    @Override
    public MetadataResponse search(SearchRequest request) throws IOException
    {
        return _client.search(request);
    }

    IStorageClient _client;
}
