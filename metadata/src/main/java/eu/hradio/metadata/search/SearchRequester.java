package eu.hradio.metadata.search;

import eu.hradio.commons.*;
import eu.hradio.metadata.*;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

/***
 * Consumes a RabbitMQ channel: delivers search requests and returns the corresponding response
  */
public class SearchRequester
{
    public class Consumer extends DefaultConsumer
    {
        public Consumer(Channel channel, HashMap<String, RequestProcessor> requests)
        {
            super(channel);

            _requests = requests;
        }

        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException
        {
            String corrId = properties.getCorrelationId();
            RequestProcessor requestProcessor = _requests.get(corrId);

            if (requestProcessor != null)
            {
                _requests.remove(corrId);

                MetadataResponse response = MetadataUtils.gson.fromJson(new String(body, "UTF-8"), MetadataResponse.class);

                requestProcessor.offerResult(response);
            }
        }

        private HashMap<String, RequestProcessor> _requests;
    }

    public static SearchRequester createServiceSearchRequester(RMQConnection rmqConnection, long timeout) throws IOException, TimeoutException
    {
        return new SearchRequester(rmqConnection, Constants.serviceQueryQueue, timeout);
    }

    public static SearchRequester createProgrammeSearchRequester(RMQConnection rmqConnection, long timeout) throws IOException, TimeoutException
    {
        return new SearchRequester(rmqConnection, Constants.programmeQueryQueue, timeout);
    }

    public static SearchRequester createScheduleSearchRequester(RMQConnection rmqConnection, long timeout) throws IOException, TimeoutException
    {
        return new SearchRequester(rmqConnection, Constants.scheduleQueryQueue, timeout);
    }

    public SearchRequester(RMQConnection rmqConnection, String requestQueue, long timeout) throws IOException, TimeoutException
    {
        this._rmqConnection = rmqConnection;
        this._requestQueue = requestQueue;
        this._timeout = timeout;

        _channel = _rmqConnection.createChannel();

        _responseQueue = _channel.queueDeclare().getQueue();

        _requests = new HashMap<String, RequestProcessor>();

        _channel.basicConsume(_responseQueue, true, new Consumer(_channel, _requests));
    }

    public MetadataResponse request(SearchRequest request) throws IOException, InterruptedException
    {
        final String corrId = UUID.randomUUID().toString();

        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(_responseQueue)
                .build();

        _channel.basicPublish("", _requestQueue, props, MetadataUtils.gson.toJson(request).getBytes("UTF-8"));

        RequestProcessor requestProcessor = new RequestProcessor();

        _requests.put(corrId, requestProcessor);

        MetadataResponse response = requestProcessor.waitOnResult(_timeout);
        if(response == null)
        {
            response = new MetadataResponse("", MetadataResponseType.TIMEOUT);
        }

        return response;
    }

    public void close() throws IOException, TimeoutException
    {
        if(_channel.isOpen())
            _channel.close();
    }

    private String _requestQueue;
    private String _responseQueue;

    private HashMap<String, RequestProcessor> _requests;

    private long _timeout;

    private RMQConnection _rmqConnection;
    private Channel _channel;
}
