package eu.hradio.metadata.search;


/***
 * Defines syntax types of Elastic Search search request query
 */
public enum SearchRequestQuerySyntax
{
    ELASTIC_SIMPLE_QUERY_STRING,
    ELASTIC_QUERY_STRING,
    ELASTIC_COMPLEX
}
