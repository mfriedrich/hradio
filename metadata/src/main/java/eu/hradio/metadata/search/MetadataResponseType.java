package eu.hradio.metadata.search;

/***
 * Enum containing valid types of metadata responses
 */
public enum MetadataResponseType
{
    OK,
    ERROR,
    NOT_FOUND,
    CREATED,
    TIMEOUT
}


