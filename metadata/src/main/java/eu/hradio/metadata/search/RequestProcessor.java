package eu.hradio.metadata.search;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/***
 * Processes requests: receives, waits for requests and offers results
 */
public class RequestProcessor
{
    public RequestProcessor()
    {
        _responseQueue = new ArrayBlockingQueue<MetadataResponse>(1);
    }

    public BlockingQueue<MetadataResponse> getResponseQueue()
    {
        return _responseQueue;
    }

    public MetadataResponse waitOnResult(long timeout) throws InterruptedException
    {
        return _responseQueue.poll(timeout, TimeUnit.MILLISECONDS);
    }

    public void offerResult(MetadataResponse result)
    {
        if(result == null)
            result = new MetadataResponse("", MetadataResponseType.TIMEOUT);

        _responseQueue.offer(result);
    }

    private BlockingQueue<MetadataResponse> _responseQueue;
}
