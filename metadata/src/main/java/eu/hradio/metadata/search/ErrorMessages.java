package eu.hradio.metadata.search;

/***
 * Contains constant values of Elastic Search errors
 */
public class ErrorMessages
{
    public static final String elasticSearchError = "ElasticSearch: Error";
    public static final String elasticSearchTimeout = "ElasticSearch: Timeout";
    public static final String elasticSearchNotFound = "ElasticSearch: Not Found";

    public static final String parseError = "Could not parse input";

}
