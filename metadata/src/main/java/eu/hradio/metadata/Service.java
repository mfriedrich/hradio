package eu.hradio.metadata;

import java.util.ArrayList;
import java.util.List;


/***
 * Contains metadata model for service
 */
public class Service implements IMetadata
{
    public Service(String id)
    {
        this.bearers = new ArrayList();
        this.genres = new ArrayList();
        this.schedules = new ArrayList();
        this.id = id;
    }


    public void addBearer(Bearer bearer)
    {
        this.bearers.add(bearer);
    }
    public void addGenre(Genre genre)
    {
        this.genres.add(genre);
    }

    public void addSchedule(Schedule schedule)
    {
        schedule.setServiceId(getId());
        this.schedules.add(schedule);
    }
    public void removeSchedule(Schedule schedule)
    {
        schedule.setServiceId(null);
        this.schedules.remove(schedule);
    }

    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name.trim();
    }

    public List<Bearer> getBearers()
    {
        return bearers;
    }
    public void setBearers(List<Bearer> bearers)
    {
        this.bearers = bearers;
    }

    public List<Genre> getGenres()
    {
        return genres;
    }
    public void setGenres(List<Genre> genres)
    {
        this.genres = genres;
    }

    public List<Schedule> getSchedules()
    {
        return schedules;
    }
    public void setSchedules(List<Schedule> schedules)
    {
        this.schedules = schedules;
    }

    @Override
    public MetadataType getType()
    {
        return MetadataType.SERVICE;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public String getParentId() { return serviceProviderId; }

    @Override
    public void setId(String id)
    {
        this.id = id;
    }

    public void setServiceProviderId(String spId)
    {
        serviceProviderId = spId;
    }
    public String getServiceProviderId()
    {
        return serviceProviderId;
    }

    private String description;
    private String name;
    private String id;
    private List<Bearer> bearers;
    private List<Genre> genres;
    private List<Schedule> schedules;
    private String serviceProviderId;
}
