#!/bin/bash

# Remote folder
remotefolder=/home/tester/hradio
remoteip=141.84.213.235
user=root

# Catch SIGINT and SIGTERM to stop dockers on remote
function control_c {
    	echo -en "\n## Caught SIGINT; Clean up and Exit \n"
    	kill_docker
	exit $?
}
trap control_c SIGINT
trap control_c SIGTERM

# Kill running docker processes.
function kill_docker() {
	echo -en "\n## Stop docker containers and remove all installed images.\n"
	ssh $user@$remoteip \
		"docker stop $(docker ps -a -q)"
	ssh $user@$remoteip \
		"docker rm $(docker ps -a -q)"
	ssh $user@$remoteip \
		"docker rmi $(docker images -q)"
}
kill_docker;

# Delete build folder.
echo -en "\n## Delete build folder on remote machine.\n"
ssh $user@$remoteip \
	"rm -rf $remotefolder/build_ext;" \

# Copy build folder from host to remote.
echo -en "\n## Copy build from host to remote machine.\n"
cp -r ./build ./build_ext;
scp -r ./build_ext $user@$remoteip:$remotefolder;
rm -r ./build_ext;

# Start new dockers.
echo -en "\n## Start platform.\n"
ssh $user@$remoteip \
	"cd $remotefolder/build_ext/; ./start.sh;"
