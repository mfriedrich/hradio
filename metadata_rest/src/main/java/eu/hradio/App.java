package eu.hradio;

import org.apache.logging.log4j.LogManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App
{
    static org.apache.logging.log4j.Logger _log = LogManager.getLogger(App.class);

    public static void main(String[] args)
    {
        _log.info("Starting REST service for metadata access.");
        SpringApplication.run(App.class, args);
    }
}