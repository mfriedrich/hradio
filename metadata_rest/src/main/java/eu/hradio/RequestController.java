package eu.hradio;

import java.io.IOException;
import java.text.ParseException;
import java.util.concurrent.TimeoutException;

import eu.hradio.commons.*;
import eu.hradio.metadata.*;

import eu.hradio.metadata.search.MetadataRequest;
import eu.hradio.metadata.search.MetadataRequestType;
import eu.hradio.metadata.search.MetadataRequester;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/***
 * Controls and handles metadata requests via a RESTful interface
 * and provides request mappings for HTTP requests
 */
@RestController
public class RequestController
{
    static org.apache.logging.log4j.Logger _log = LogManager.getLogger(RequestController.class);

    public RequestController(@Value("${rabbitmq.address}") String rmqAddress, @Value("${rabbitmq.port}") int rmqPort,
                             @Value("${rabbitmq.user}") String rmqUser, @Value("${rabbitmq.password}") String rmqPassword,
                             @Value("${rabbitmq.retries}") int retries, @Value("${rabbitmq.retryInterval}") int retryInterval,
                             @Value("${rest.requestTimeout}") int requestTimeout) throws IOException, TimeoutException, InterruptedException
    {
        RMQConnection rmqCon = new RMQConnection(new RMQConfig(rmqAddress, rmqUser, rmqPassword, rmqPort, retries, retryInterval));

        _metadataRequester = new MetadataRequester(rmqCon, requestTimeout);
    }


    // GET

    @RequestMapping(value="/providers", method=RequestMethod.GET)
    public ResponseEntity<?> requestProviders() throws IOException, InterruptedException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("","", "",MetadataRequestType.GET, MetadataType.SERVICE_PROVIDER));
    }

    @RequestMapping(value="/providers/{providerId}", method=RequestMethod.GET)
    public ResponseEntity<?> requestProvider(@PathVariable String providerId) throws IOException, InterruptedException
    {
        return ResultConverter.request(_metadataRequester, new MetadataRequest("", providerId, "", MetadataRequestType.GET, MetadataType.SERVICE_PROVIDER));
    }

    @RequestMapping(value="/providers/{providerId}/services", method=RequestMethod.GET)
    public ResponseEntity<?> requestServices(@PathVariable String providerId) throws IOException, InterruptedException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("", "", providerId, MetadataRequestType.GET, MetadataType.SERVICE));
    }

    @RequestMapping(value="/providers/{providerId}/services/{serviceId}", method=RequestMethod.GET)
    public ResponseEntity<?> requestService(@PathVariable String providerId, @PathVariable String serviceId) throws IOException, InterruptedException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("", serviceId ,"", MetadataRequestType.GET, MetadataType.SERVICE));
    }

    @RequestMapping(value="/providers/{providerId}/services/{serviceId}/schedules", method=RequestMethod.GET)
    public ResponseEntity<?> requestSchedules(@PathVariable String providerId, @PathVariable String serviceId) throws IOException, InterruptedException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("", "", serviceId, MetadataRequestType.GET, MetadataType.SCHEDULE));
    }

    @RequestMapping(value="/providers/{providerId}/services/{serviceId}/schedules/{scheduleId}", method=RequestMethod.GET)
    public ResponseEntity<?> requestSchedule(@PathVariable String providerId, @PathVariable String serviceId, @PathVariable String scheduleId) throws IOException, InterruptedException, ParseException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("", scheduleId,"", MetadataRequestType.GET, MetadataType.SCHEDULE));
    }

    @RequestMapping(value="/providers/{providerId}/services/{serviceId}/schedules/{scheduleId}/programmes", method=RequestMethod.GET)
    public ResponseEntity<?> requestProgrammes(@PathVariable String providerId, @PathVariable String serviceId, @PathVariable String scheduleId) throws IOException, InterruptedException, ParseException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("", "",scheduleId, MetadataRequestType.GET, MetadataType.PROGRAMME));
    }

    @RequestMapping(value="/providers/{providerId}/services/{serviceId}/schedules/{scheduleId}/programmes/{programmeId}", method=RequestMethod.GET)
    public ResponseEntity<?> requestProgramme(@PathVariable String providerId, @PathVariable String serviceId, @PathVariable String scheduleId, @PathVariable String programmeId) throws IOException, InterruptedException, ParseException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("", programmeId,scheduleId, MetadataRequestType.GET, MetadataType.PROGRAMME));
    }


    // DELETE

    @RequestMapping(value="/providers/{providerId}/services/{serviceId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteService(@PathVariable String providerId, @PathVariable String serviceId) throws IOException, InterruptedException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("",  serviceId,"", MetadataRequestType.DELETE, MetadataType.SERVICE));
    }

    @RequestMapping(value="/providers/{providerId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteServiceProvider(@PathVariable String providerId) throws IOException, InterruptedException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("",  providerId,"", MetadataRequestType.DELETE, MetadataType.SERVICE_PROVIDER));
    }

    @RequestMapping(value="/providers/{providerId}/services/{serviceId}/schedules/{scheduleId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteSchedule(@PathVariable String providerId, @PathVariable String serviceId, @PathVariable String scheduleId) throws IOException, InterruptedException, ParseException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("",  scheduleId,"", MetadataRequestType.DELETE, MetadataType.SCHEDULE));
    }

    @RequestMapping(value="/providers/{providerId}/services/{serviceId}/schedules/{scheduleId}/programmes/{programmeId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteProgramme(@PathVariable String providerId, @PathVariable String serviceId, @PathVariable String scheduleId, @PathVariable String programmeId) throws IOException, InterruptedException, ParseException
    {
        return ResultConverter.request(_metadataRequester,new MetadataRequest("", programmeId,"", MetadataRequestType.DELETE, MetadataType.PROGRAMME));
    }


    // PUT

    @RequestMapping(value="/providers", method=RequestMethod.PUT)
    public ResponseEntity<?> createServiceProvider(@RequestBody String provider) throws IOException, InterruptedException
    {
        _log.info("Provider PUT");
        return ResultConverter.request(_metadataRequester,new MetadataRequest(provider, "","", MetadataRequestType.CREATE, MetadataType.SERVICE_PROVIDER));
    }

    @RequestMapping(value="/providers/{providerId}/services", method=RequestMethod.PUT)
    public ResponseEntity<?> createService(@PathVariable String providerId, @RequestBody String service) throws IOException, InterruptedException
    {
        _log.info("Service PUT");
        return ResultConverter.request(_metadataRequester,new MetadataRequest(service, "", providerId, MetadataRequestType.CREATE, MetadataType.SERVICE));
    }

    @RequestMapping(value="/providers/{providerId}/services/{serviceId}/schedules", method=RequestMethod.PUT)
    public ResponseEntity<?> createSchedule(@PathVariable String providerId, @PathVariable String serviceId, @RequestBody String schedule) throws IOException, InterruptedException
    {
        _log.info("Schedule PUT");
        return ResultConverter.request(_metadataRequester,new MetadataRequest(schedule, "", serviceId, MetadataRequestType.CREATE, MetadataType.SCHEDULE));
    }

    @RequestMapping(value="/providers/{providerId}/services/{serviceId}/schedules/{scheduleId}/programmes", method=RequestMethod.PUT)
    public ResponseEntity<?> createProgramme(@PathVariable String providerId, @PathVariable String serviceId, @PathVariable String scheduleId, @RequestBody String programme) throws IOException, InterruptedException, ParseException
    {
        _log.info("Programme PUT");

        return ResultConverter.request(_metadataRequester, new MetadataRequest(programme, "",scheduleId, MetadataRequestType.CREATE, MetadataType.PROGRAMME));
    }

    private MetadataRequester _metadataRequester;
}