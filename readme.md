Step 1: Install JDK 8 http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

Step 2: Install gradle https://gradle.org/install/

Step 3: Install Docker https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce-1

Step 4: Install Docker Compose https://docs.docker.com/compose/install/#install-compose

Step 5: run do_all.sh script to build code base and execute docker containers. 


Get Container IP: 
-----------------
docker ps
docker inspect <container ID>


Webfrontends:
-------------
- RabbitMQ: http://localhost:15672/
- ES: http://localhost:9200/

Elastic Search Issues: 
----------------------

- Cannot start because of too low max_map_count. Solution: sudo sysctl -w vm.max_map_count=262144
- Start search java app. Exception: Exception in thread "main" java.lang.IllegalStateException: path.home is not configured
