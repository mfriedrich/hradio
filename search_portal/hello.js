$(document).ready(function() {
   

    $("#searchButton").click(function() {
        serviceIsChecked = $('#service').prop('checked');
	    programmeIsChecked = $('#programme').prop('checked');

	    if (!serviceIsChecked && !programmeIsChecked) {
	        $('#infoField').text = "Please choose a search type.";
	        return;
        }

        mapping = serviceIsChecked ? "services" : "programmes";
        console.log(mapping)
        searchQueryURL = "http://localhost:8080/api/v1/"+mapping+"?q=" + $('#searchInput').val();
        console.log(searchQueryURL)
      
	$.getJSON(searchQueryURL,
	   function(data) {
	    //strData = JSON.stringify(data);
	  
	    //jsons = JSON.parse(d);
            
            console.log(JSON.stringify(data));

	    serviceIsChecked = $('#service').prop('checked');
	    programmeIsChecked = $('#programme').prop('checked');

        if (serviceIsChecked) {

            $('#result tr').remove()
            table = '<tr>';
	    
            table += '<td style="color:#264d73;font-weight: bold">Name</td>'               
            table += '<td style="color:#264d73;font-weight: bold">Bearers</td>'
            table += '<td style="color:#264d73;font-weight: bold">Genres</td>'
           
            table += '/<tr>';
            
             $('#result').append(table);

        for (i in data) { 
              
            table = '<tr>';
             
            table += '<td style="color:#264d73">' + data[i].content.name + '</td>';
             
            openUl = '<ul>', closeUl = '</ul>';
            bearers = '';

            for (j in data[i].content.bearers) {
                bearers += '<li>' +  data[i].content.bearers[j].address + '</li> ';
            }
	    table += '<td style="color:#264d73">' + bearers + '</td>';
            
              
  	    genres = ''
 	    for (j in data[i].content.genres) {
                genres += '<li>' +  data[i].content.genres[j].name + '</li> ';
            }
	    table += '<td style="color:#264d73">' + genres + '</td>';
          
           
          
	    table += '</tr>';
	    
            $('#result tr:last')
            .after(table);

            
               
         }
         }

         else {
            $('#result tr').remove()
            table = '<tr>';
	    
            table += '<td style="color:#264d73;font-weight: bold">Name</td>'               
            table += '<td style="color:#264d73;font-weight: bold">Description</td>'
            table += '<td style="color:#264d73;font-weight: bold">Bearers</td>'
            table += '<td style="color:#264d73;font-weight: bold">Start</td>'
            table += '<td style="color:#264d73;font-weight: bold">Stop</td>'
          
            table += '/<tr>';
            
             $('#result').append(table);

           

         for (i in data) { 
              
            table = '<tr>';
             
            table += '<td style="color:#264d73">' + data[i].content.name + '</td>';
            table += '<td style="color:#264d73">' + (typeof data[i].content.description !== 'undefined'? data[i].content.description : '' )+ '</td>';
            
            openUl = '<ul>', closeUl = '</ul>';
            bearers = '';

            for (j in data[i].content.bearers) {
                bearers += '<li>' +  data[i].content.bearers[j].address + '</li> ';
            }
	    table += '<td style="color:#264d73">' + bearers + '</td>';
            
                
 	    table += '<td style="color:#264d73">' + data[i].content.startTime + '</td>';
            table += '<td style="color:#264d73">' + data[i].content.stopTime + '</td>';
           
          
	    table += '</tr>';
	    
            $('#result tr:last')
            .after(table);

            
               
         }

}
	   });


      /*$.ajax({
        url: searchQueryURL
      }).then(function(data) {
         alert(data);
         $('#searchResult').append(data.content);
         });*/
    });
});
