package eu.hradio;

import eu.hradio.commons.*;

import eu.hradio.metadata.Constants;
import eu.hradio.metadata.storage.IStorageClient;
import eu.hradio.metadata.storage.elastic.ESConfig;
import eu.hradio.metadata.storage.elastic.ESStorageClient;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class App implements CommandLineRunner
{
    static org.apache.logging.log4j.Logger _log = LogManager.getLogger(App.class);

    @Autowired
    private ConfigurableApplicationContext context;

    @Value("${rabbitmq.address}")
    String rmqAddress;
    @Value("${rabbitmq.port}")
    int rmqPort;
    @Value("${rabbitmq.user}")
    String rmqUser;
    @Value("${rabbitmq.password}")
    String rmqPassword;
    @Value("${rabbitmq.retries}")
    int rmqRetries;
    @Value("${rabbitmq.retryInterval}")
    int rmqRetryInterval;

    @Value("${elastic.protocol}")
    String esProtocol;
    @Value("${elastic.address}")
    String esAddress;
    @Value("${elastic.port}")
    int esPort;
    @Value("${elastic.requestTimeout}")
    int esRequestTimeout;
    @Value("${elastic.retries}")
    int esRetries;
    @Value("${elastic.retryInterval}")
    int esRetryInterval;
    @Value("${elastic.deleteIndicesIfExisting}")
    boolean esDeleteIndicesIfExisting;

    @Override
    public void run(String... args)
    {
        try
        {
            _log.info("Setup Elastic Search connection...");
            ESConfig elasticConfig = new ESConfig(esProtocol, esAddress, esPort, esRequestTimeout, esRetries, esRetryInterval, esDeleteIndicesIfExisting);
            IStorageClient storageClient = new ESStorageClient(elasticConfig, true);
            _log.info("Done.");

            _log.info("Setup RabbitMQ connection...");
            RMQConnection rmqCon = new RMQConnection(new RMQConfig(rmqAddress, rmqUser, rmqPassword, rmqPort, rmqRetries, rmqRetryInterval));
            _log.info("Done.");

            DLMetadataManager dlMM = new DLMetadataManager(rmqCon, storageClient);

            DNSMetadataManager dnsMM = new DNSMetadataManager(rmqCon, storageClient);

            MetadataRequestHandler mdrHandler = new MetadataRequestHandler(rmqCon, storageClient, Constants.metadataRequestQueue);

            Runtime.getRuntime().addShutdownHook(new Thread()
            {
                public void run()
                {
                    try
                    {
                        dlMM.close();
                        dnsMM.close();
                        mdrHandler.close();

                        rmqCon.close();
                        storageClient.close();

                        _log.info("User-triggered shutdown.");

                    } catch (Exception ex)
                    {
                        _log.error(ex.getMessage());
                    }
                }
            });

        } catch (Exception ex)
        {
            _log.fatal(ex.getMessage());
            exitWithError();
        }
    }

    private void exitWithError()
    {
        context.close();
        System.exit(1);
    }

    public static void main(String[] args)
    {
        SpringApplication app = new SpringApplication(App.class);
        //app.setBannerMode(Banner.Mode.OFF);
        app.run(args);

    }


}

