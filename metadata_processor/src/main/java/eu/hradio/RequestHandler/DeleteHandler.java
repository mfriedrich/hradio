package eu.hradio.RequestHandler;

import eu.hradio.metadata.search.MetadataRequest;
import eu.hradio.metadata.search.MetadataResponse;
import eu.hradio.metadata.search.MetadataResponseType;
import eu.hradio.metadata.storage.IStorageClient;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;


/***
 * Deletes a Metadata item in Elastic Search
 */
public class DeleteHandler implements IRequestHandler
{
    private static org.apache.logging.log4j.Logger _log = LogManager.getLogger(DeleteHandler.class);

    @Override
    public MetadataResponse handle(MetadataRequest request, IStorageClient client) throws IOException
    {
        if(request.getItemId().isEmpty())
        {
            //We only support per-item deletes (not per item collection).
            return new MetadataResponse("", MetadataResponseType.ERROR);
        }
        else
        {
            MetadataResponseType res = client.delete(request.getItemId(), request.getMetadataType());

            return new MetadataResponse("", res);
        }
    }
}
