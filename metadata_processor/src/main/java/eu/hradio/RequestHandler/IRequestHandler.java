package eu.hradio.RequestHandler;

import eu.hradio.metadata.search.MetadataRequest;
import eu.hradio.metadata.search.MetadataResponse;
import eu.hradio.metadata.storage.IStorageClient;

import java.io.IOException;


/***
 * Interface defining the structure of a request handler
 */
public interface IRequestHandler
{
    MetadataResponse handle(MetadataRequest request, IStorageClient client) throws IOException;
}
