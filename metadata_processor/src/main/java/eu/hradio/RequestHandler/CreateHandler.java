package eu.hradio.RequestHandler;

import com.google.gson.*;
import eu.hradio.metadata.*;
import eu.hradio.metadata.search.ErrorMessages;
import eu.hradio.metadata.search.MetadataRequest;
import eu.hradio.metadata.search.MetadataResponse;
import eu.hradio.metadata.search.MetadataResponseType;
import eu.hradio.metadata.storage.IStorageClient;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;


/***
 * Creates a Metadata item in Elastic Search
 */
public class CreateHandler implements IRequestHandler
{
    private static org.apache.logging.log4j.Logger _log = LogManager.getLogger(CreateHandler.class);

    @Override
    public MetadataResponse handle(MetadataRequest request, IStorageClient client) throws IOException
    {
        MetadataType type = request.getMetadataType();

        try
        {
            JsonElement itemJson = MetadataUtils.jsonParser.parse(request.getContent());
            IMetadata item = client.getMetadataCreator(type).createMetadataObject(MetadataUtils.gson, itemJson, request);

            MetadataResponseType res = client.create(item);

            if(res == MetadataResponseType.CREATED)
            {
                _log.info(String.format("Created metadata item '%s' of type %s.", item.getId(), item.getType().toString()));
                return new MetadataResponse(MetadataUtils.gson.toJson(item), MetadataResponseType.CREATED);
            }
            else
                return new MetadataResponse("", MetadataResponseType.ERROR);
        }
        catch (RuntimeException ex)
        {
            _log.error(String.format("Creating a %s metadata item caused a RuntimeException: %s", type.toString(), ex.getMessage()));
            return new MetadataResponse(ErrorMessages.parseError, MetadataResponseType.ERROR);
        }
    }
}
