package eu.hradio.RequestHandler;

import eu.hradio.metadata.*;
import eu.hradio.metadata.search.MetadataRequest;
import eu.hradio.metadata.search.MetadataResponse;
import eu.hradio.metadata.search.MetadataResponseType;
import eu.hradio.metadata.storage.IStorageClient;

import java.io.IOException;
import java.util.List;


/***
 * Gets a Metadata item in Elastic Search
 */
public class GetHandler implements IRequestHandler
{
    @Override
    public MetadataResponse handle(MetadataRequest request, IStorageClient client) throws IOException
    {
        MetadataType type = request.getMetadataType();

        if(request.getItemId().isEmpty())
        {
            List<IMetadata> items = client.getChilds(request.getParentId(), type);
            if(items == null)
                return new MetadataResponse("", MetadataResponseType.NOT_FOUND);
            else
                return new MetadataResponse(MetadataUtils.gson.toJson(items), MetadataResponseType.OK);
        }
        else
        {
            IMetadata item = client.get(request.getItemId(), type);
            if(item == null)
                return new MetadataResponse("", MetadataResponseType.NOT_FOUND);
            else
                return new MetadataResponse(MetadataUtils.gson.toJson(item), MetadataResponseType.OK);
        }
    }
}
