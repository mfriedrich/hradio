package eu.hradio;

import com.rabbitmq.client.*;
import eu.hradio.commons.RMQConnection;
import eu.hradio.metadata.Constants;
import eu.hradio.metadata.storage.IStorageClient;
import eu.hradio.metadata.IMetadata;
import eu.hradio.metadata.search.MetadataResponseType;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;


/***
 * Handles DNS data:
 * Consumes data of metadata queue, delivers incoming XMLs to DNSMetadataParser and
 * creates results in Elastic Search
 */
public class DNSMetadataManager
{
    public DNSMetadataManager(RMQConnection rmqConnection, IStorageClient storageClient) throws IOException
    {
        this._rmqConnection = rmqConnection;
        this._storageClient = storageClient;

        _channel = _rmqConnection.createChannel();
        _channel.queueDeclare(_metadataQueue, false, false, false, null);

        Consumer consumer = new DefaultConsumer(_channel)
        {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body)
                    throws IOException
            {
                String message = new String(body, "UTF-8");

                List<IMetadata> metadata = null;
                try
                {
                    metadata = new DNSMetadataParser(message).parse();
                } catch (Exception ex)
                {
                    _log.error("Could not parse RadioDNS Metadata. Reason:" + ex.getMessage());
                }

                if(metadata == null)
                    return;

                for (IMetadata md : metadata)
                {
                    try
                    {
                        MetadataResponseType res = _storageClient.create(md);
                        if(res != MetadataResponseType.CREATED)
                            _log.error(String.format("Could not create RadioDNS Metadata item '%s' of type %s.",md.getId(), md.getType().toString()));

                    } catch (IOException ex)
                    {
                        _log.error(String.format("Could not create RadioDNS Metadata item '%s' of type %s . Reason: %s",md.getId(), md.getType().toString(), ex.getMessage()));
                    }
                }
            }
        };

        _channel.basicConsume(_metadataQueue, true, consumer);
    }


    public void close() throws IOException, TimeoutException
    {
        if(_channel.isOpen())
            _channel.close();
    }

    private static org.apache.logging.log4j.Logger _log = LogManager.getLogger(DNSMetadataManager.class);

    private RMQConnection _rmqConnection;
    private Channel _channel;
    private final String _metadataQueue = Constants.rdnsMetadataQueue;
    private IStorageClient _storageClient;
}
