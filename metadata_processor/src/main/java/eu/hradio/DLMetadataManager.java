package eu.hradio;

import com.google.gson.*;
import com.rabbitmq.client.*;

import eu.hradio.commons.CommonUtils;
import eu.hradio.commons.RMQConnection;

import eu.hradio.metadata.*;
import eu.hradio.metadata.search.MetadataResponse;
import eu.hradio.metadata.search.MetadataResponseType;
import eu.hradio.metadata.search.SearchRequestQuerySyntax;
import eu.hradio.metadata.storage.IStorageClient;
import eu.hradio.metadata.storage.elastic.ESStorageClient;
import org.apache.logging.log4j.LogManager;

import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContent;
import org.elasticsearch.index.query.*;
import org.elasticsearch.node.Node;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.concurrent.TimeoutException;


/***
 * Handles Dynamic Label data:
 * Consumes data of dl_metadata queue, creates corresponding ProgrammeEvent objects and
 * adds these to the matching Programme in Elastic Search
 */
public class DLMetadataManager
{
    public DLMetadataManager(RMQConnection rmqConnection, IStorageClient client) throws IOException
    {
        this._rmqConnection = rmqConnection;

        _storageClient = client;

        if (!(_storageClient instanceof ESStorageClient))
            throw new InternalError("DLMetadataManager is only supported for Elastic Search clients.");

        _channel = _rmqConnection.createChannel();
        _channel.queueDeclare(_metadataQueue, false, false, false, null);

        Consumer consumer = new DefaultConsumer(_channel)
        {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body)
                    throws IOException
            {
                try
                {
                    String message = new String(body, "UTF-8");

                    JSONObject dynamicLabel = (JSONObject) (new JSONObject(message)).get("DynamicLabel");
                    _log.info(String.format("Received frame: %s", dynamicLabel.toString()));

                    cacheEvent(dynamicLabel);

                    if (eventStopped(dynamicLabel))
                    {
                        int serviceId = dynamicLabel.getInt("ServiceId");
                        ProgrammeEvent finishedEvent = programmeEvents.get(serviceId);

                        finishedEvent.setStopTime(new DateTime(DateTimeZone.UTC));
                        _log.info(String.format("Programme event %d finished at %s ", serviceId, MetadataUtils.sdf_dateFormat.format(finishedEvent.getStopTime())));


                        int ensembleECC = dynamicLabel.getInt("EnsembleEcc");
                        int ensembleId = dynamicLabel.getInt("EnsembleId");
                        int sId = dynamicLabel.getInt("ServiceId");
                        Programme programme = getParent(ensembleECC, ensembleId, sId, programmeEvents.get(sId));

                        if (programme == null)
                        {
                            _log.info("No matching programme was found. Programme event cannot be stored!");
                        } else
                        {
                            _log.info(String.format("Found parent programme %s", programme.getId()));
                            storeProgramme(programme);
                        }

                        programmeEvents.remove(serviceId);
                        if (shouldBeCached(finishedEvent, dynamicLabel))
                        {
                            cacheEvent(dynamicLabel);
                        }
                    }
                } catch (Exception ex)
                {
                    ex.printStackTrace();
                    _log.error(String.format("Exception message: %s", ex.getMessage()));
                }
            }
        };

        _channel.basicConsume(_metadataQueue, true, consumer);
    }


    private boolean shouldBeCached(ProgrammeEvent event, JSONObject dynamicLabel)
    {
        if (event == null || event.getContents().size() == 0)
            return true;

        JSONArray contents = dynamicLabel.getJSONArray("Tags");

        for (Object elem : contents)
        {
            JSONObject json = (JSONObject) elem;
            if (json.has("ContentTypeName") && json.has("TagText"))
            {
                String contentType = json.getString("ContentTypeName");
                String contentValue = json.getString("TagText");
                boolean found = false;
                for (Content content : event.getContents())
                {
                    if (content.getType().equalsIgnoreCase(contentType) &&
                            content.getValue().equalsIgnoreCase(contentValue))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found) return true;
            }
        }

        return false;
    }


    private ProgrammeEvent createEvent(JSONObject dlPayload)
    {
        ProgrammeEvent event = new ProgrammeEvent(new DateTime(DateTimeZone.UTC));

        _log.info("Programme event started at " + MetadataUtils.sdf_dateFormat.format(event.getStartTime()));

        JSONArray tags = dlPayload.getJSONArray("Tags");
        for (int i = 0; i < tags.length(); i++)
        {
            JSONObject tag = (JSONObject) tags.get(i);
            String contentType = tag.getString("ContentTypeName");
            String contentValue = tag.getString("TagText");
            Content content = new Content(contentType, contentValue);

            event.addContent(content);
        }

        return event;
    }

    /**
     * determines whether toggle bit changed
     *
     * @param dlPayload JSONObject containing DL data
     * @return true if toggle bit changed, otherwise false
     */
    private boolean eventStopped(JSONObject dlPayload)
    {
        Integer serviceId = dlPayload.getInt("ServiceId");
        Boolean toggleBit = dlPayload.getBoolean("ItemToggle");

        return eventStopped(serviceId, toggleBit);
    }


    private boolean eventStopped(int serviceId, boolean toggleBit)
    {
        if (toggleBits.isEmpty()) return false;
        return toggleBit != toggleBits.get(serviceId);
    }

    private boolean isRunning(JSONObject dl)
    {
        return dl.getBoolean("ItemRunning");
    }

    public boolean cacheEvent(JSONObject dlPayload)
    {
        return cacheEvent(dlPayload, false);
    }

    /**
     * generates ProgrammeEvent object and adds it to hash table
     *
     * @param dlPayload JSONObject containing DL information
     */
    public boolean cacheEvent(JSONObject dlPayload, boolean force)
    {
        Integer key = dlPayload.getInt("ServiceId");


        // This ProgrammeEvent object was already created,
        // => wait for Toggle Bit to change state to get StopTime for it
        if (programmeEvents.containsKey(key) && !force)
        {
            return false;
        }

        ProgrammeEvent event = createEvent(dlPayload);
        this.programmeEvents.put(key, event);
        this.toggleBits.put(key, dlPayload.getBoolean("ItemToggle"));

        return true;
    }


    private Programme getParent(int ensembleEcc, int ensembleId, int sId, ProgrammeEvent event)
    {
        Programme parent = null;
        try
        {
            String url = MetadataUtils.generatePIUrl(ensembleEcc, ensembleId, sId);
            if (!CommonUtils.isAccessible(url))
                _log.warn(String.format("The url %s is not accessible", url));

            int startIndex = url.indexOf("/dab/") + 1;
            int endIndex = url.lastIndexOf('/');
            String dabAddress = url.substring(startIndex, endIndex)
                    .replaceFirst("/", ":")
                    .replace('/', '.');

            _log.info(String.format("Dab address: %s", dabAddress));

            String serviceId = getMetadataServiceId(dabAddress);

            if (serviceId.equals("NOT_FOUND")) return null;

            parent = getMetadataProgramme(serviceId,
                    event.getStartTime(),
                    event.getStopTime());

            if (parent != null) {
                if(parent.getProgrammeEvents() == null)
                    parent.setProgrammeEvents(new ArrayList<ProgrammeEvent>());
                parent.addProgrammeEvent(event);
            }

        } catch (IOException e)
        {
            e.printStackTrace();
            _log.error(String.format("Exception message: %s", e.getMessage()));
        }

        return parent;
    }

    private String getMetadataServiceId(String dabAddress) throws IOException
    {
        String serviceId;

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        NestedQueryBuilder getService = QueryBuilders.nestedQuery("bearers",
                QueryBuilders.matchQuery("bearers.address", dabAddress), ScoreMode.Max);
        sourceBuilder.query(getService);

        String queryString = new JSONObject(sourceBuilder.toString()).getJSONObject("query").toString();

        eu.hradio.metadata.search.SearchRequest sr = new eu.hradio.metadata.search.SearchRequest(MetadataType.SERVICE,
                queryString, 1, 0, SearchRequestQuerySyntax.ELASTIC_COMPLEX);
        MetadataResponse queryResponse = _storageClient.search(sr);

        if (queryResponse.getType().equals(MetadataResponseType.NOT_FOUND))
        {
            _log.error(String.format("Service with DAB %s not found", dabAddress));
            return "NOT_FOUND";
        }
        //Not found is ok since it is possible that no children are available.
        if (queryResponse.getType() != MetadataResponseType.OK)
            throw new IOException("QUERY STATUS: " + queryResponse.getType().toString() +
                    ", \nCONTENT:" + queryResponse.getContent());

        JsonArray children = (JsonArray) MetadataUtils.jsonParser.parse(queryResponse.getContent());

        _log.info(String.format("Matching service found: %s", children.toString()));

        JsonElement child = children.get(0).getAsJsonObject().get("content");

        Service service = MetadataUtils.gson.fromJson(child, Service.class);
        serviceId = service.getId();

        return serviceId;
    }


    /**
     * searches Programme containing ProgrammeEvent in ES
     *
     * @param serviceId      id of service for programme
     * @param eventStart start date of event
     * @param eventStop  stop date of event
     * @return Programme object
     */
    private Programme getMetadataProgramme(String serviceId, Date eventStart, Date eventStop)
            throws IOException
    {
        Programme programme;

        // format date with correct pattern
        String startTime = MetadataUtils.sdf_dateFormat.format(eventStart);
        String stopTime = MetadataUtils.sdf_dateFormat.format(eventStop);

        BoolQueryBuilder bq = QueryBuilders.boolQuery();
        bq.must(QueryBuilders.termQuery("serviceId", serviceId));
        bq.must(QueryBuilders.rangeQuery("startTime").lte(startTime));
        bq.must(QueryBuilders.rangeQuery("stopTime").gte(stopTime));

        _log.info("Get programme for programme part with start time " + startTime + " and stop time " + stopTime);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        sourceBuilder.query(bq);

        String queryString = new JSONObject(sourceBuilder.toString()).getJSONObject("query").toString();

        eu.hradio.metadata.search.SearchRequest sr = new eu.hradio.metadata.search.SearchRequest(MetadataType.PROGRAMME,
                queryString, 1, 0, SearchRequestQuerySyntax.ELASTIC_COMPLEX);
        MetadataResponse queryResponse = _storageClient.search(sr);

        if (queryResponse.getType().equals(MetadataResponseType.NOT_FOUND))
            return null;
        //Not found is ok since it is possible that no children are available.
        if (queryResponse.getType() != MetadataResponseType.OK)
            throw new IOException("QUERY STATUS: " + queryResponse.getType().toString() +
                    ", \nCONTENT:" + queryResponse.getContent());

        JsonArray children = (JsonArray) MetadataUtils.jsonParser.parse(queryResponse.getContent());

        _log.info(String.format("Matching programme found: %s", children.toString()));

        JsonElement child = children.get(0).getAsJsonObject().get("content");

        programme = MetadataUtils.gson.fromJson(child, Programme.class);

        return programme;
    }


    public void close() throws IOException, TimeoutException
    {
        if (_channel.isOpen())
            _channel.close();

    }

    private void storeProgramme(Programme programme)
    {
        try
        {
            String json = MetadataUtils.toJSONString(programme);

            String index = MetadataUtils.getESIndexFor(MetadataType.PROGRAMME);
            _log.info(String.format("Store programme at %s (%s).", index, json));

            MetadataResponseType res = _storageClient.create(programme);

            if (res != MetadataResponseType.CREATED)
                _log.error(String.format("Could not create RadioDNS Metadata item '%s' of type %s.", programme.getId(), programme.getType().toString()));
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    private Node _node;
    private IStorageClient _storageClient;

    RMQConnection _rmqConnection;

    private Channel _channel;
    private static org.apache.logging.log4j.Logger _log = LogManager.getLogger(DLMetadataManager.class);
    private final String _metadataQueue = Constants.dlMetadataQueue;

    // Storage for incomplete ProgrammeEvents
    private Hashtable<Integer, ProgrammeEvent> programmeEvents = new Hashtable<>();

    // Storage for current value of Toggle Bit for corresponding service
    private Hashtable<Integer, Boolean> toggleBits = new Hashtable<>();

}
