package eu.hradio;

import eu.hradio.commons.CommonUtils;
import eu.hradio.metadata.*;
import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.ReadableDuration;
import org.joda.time.format.ISOPeriodFormat;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static eu.hradio.commons.CommonUtils.onlyConsistsOfSpecialChars;


/***
 * Parses through XMLs containing DNS data and creates objects of type
 * ServiceProvider, Service, Schedule and Programme
 */
public class DNSMetadataParser
{
    private String content;

    private Document document;

    public DNSMetadataParser(String xmlContent) throws ParserConfigurationException, org.xml.sax.SAXException, IOException
    {
        this.content = xmlContent;

        InputStream in = new ByteArrayInputStream(this.content.getBytes());
        this.document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(in);
    }

    public String getXmlContent()
    {
        return this.content;
    }

    public List<IMetadata> parse()
    {
        List<IMetadata> items = new ArrayList<>();

        NodeList root = this.document.getChildNodes();

        //collect service provider information from serviceInformation-tag
        Node siNode = getNode("serviceInformation", root);

        if (siNode == null)
            return null;

        NodeList services = getNode("services", siNode.getChildNodes()).getChildNodes();

        ServiceProvider provider = null;

        //Parse ServiceProvider
        for (int i = 0; i < services.getLength(); i++)
        {
            Node serviceNode = services.item(i);

            switch (serviceNode.getNodeName().toLowerCase())
            {
                case "serviceprovider":

                    provider = parseServiceProvider(serviceNode);
                    if(provider != null)
                    {
                        items.add(provider);
                    }

                    break;
            }
        }

        //Parse Services
        for (int i = 0; i < services.getLength(); i++)
        {
            Node serviceNode = services.item(i);

            switch (serviceNode.getNodeName().toLowerCase())
            {
                case "service":

                    Service service = parseService(serviceNode, provider, items);
                    if(service != null)
                    {
                        items.add(service);
                        //sp.addService(service);
                    }

                    break;
            }
        }
        return items;
    }

    public ServiceProvider parseServiceProvider(Node serviceProviderNode)
    {
        String name = getNodeValue("shortName", serviceProviderNode.getChildNodes());
        if(name == null || name.isEmpty())
            return null;

        String description = getNodeValue("description", serviceProviderNode.getChildNodes());

        String id = getNodeAttr("id", serviceProviderNode);

        // if no Id tag is there then compute hash Id of name
        if (id.equalsIgnoreCase(""))
            id = MetadataUtils.computeId(name);

        ServiceProvider provider = new ServiceProvider(id);

        provider.setDescription(description);
        provider.setName(name);

        return provider;
    }

    public Service parseService(Node serviceNode, ServiceProvider provider, List<IMetadata> items)
    {
        NodeList serviceChildNodes = serviceNode.getChildNodes();

        String name = null;
        ArrayList<Genre> genres = new ArrayList<>();
        ArrayList<Bearer> bearers = new ArrayList<>();

        Node scheduleNode = null;

        for (int i3 = 0; i3 < serviceChildNodes.getLength(); i3++)
        {
            Node serviceValue = serviceChildNodes.item(i3);

            switch (serviceValue.getNodeName().toLowerCase())
            {
                case "mediumname":
                    name = getNodeValue(serviceValue);
                    break;

                case "genre":
                    genres.add(new Genre(getNodeValue(serviceValue)));
                    break;

                case "bearer":
                    bearers.add(new Bearer(getNodeAttr("id", serviceValue)));
                    break;

                case "schedule":
                    scheduleNode = serviceValue;
                    break;
            }
        }

        if(name == null || name.isEmpty())
            return null;

        Service service = new Service(MetadataUtils.computeId(name));
        service.setName(name);
        service.setBearers(bearers);
        service.setGenres(genres);

        if (provider != null)
        {
            service.setServiceProviderId(provider.getId());
            //provider.addService(service);
        }

        if(scheduleNode != null)
        {
            Schedule schedule = parseSchedule(scheduleNode, service, items);
            if (schedule != null)
            {
                items.add(schedule);
            }
        }

        return service;
    }


    public Schedule parseSchedule(Node scheduleNode, Service service, List<IMetadata> items)
    {
        NodeList scheduleChildNodes = scheduleNode.getChildNodes();

        String id = MetadataUtils.computeId(getNodeAttr("creationtime", scheduleNode) + service.getId()); //getNodeAttr("originator", scheduleNode));
        if(id.isEmpty())
            return null;

        Schedule schedule = new Schedule(id);
        schedule.setServiceId(service.getId());

        for (int i1 = 0; i1 < scheduleChildNodes.getLength(); i1++)
        {
            Node scheduleChildNode = scheduleChildNodes.item(i1);

            switch (scheduleChildNode.getNodeName().toLowerCase())
            {
                case "scope":
                    String strStart = getNodeAttr("starttime", scheduleChildNode);
                    String strStop = getNodeAttr("stoptime", scheduleChildNode);

                    schedule.setStartTime((Instant.parse(strStart)).toDate());      //convert to Date using Instant because then UTC time is used
                    schedule.setStopTime((Instant.parse(strStop)).toDate());

                    NodeList scopeNodes = scheduleChildNode.getChildNodes();

                    for (int i2 = 0; i2 < scopeNodes.getLength(); i2++)
                    {
                        Node scope = scopeNodes.item(i2);
                        if (scope.getNodeName().equalsIgnoreCase("servicescope"))
                        {
                            schedule.addScope(new Scope(getNodeAttr("id", scope)));
                        }
                    }

                    break;

                case "programme":

                    Programme programme = parseProgramme(scheduleChildNode);
                    if(programme != null)
                    {
                        programme.setBearers(service.getBearers());
                        programme.setServiceId(service.getId());
                        programme.setScheduleId(schedule.getId());

                        //schedule.addProgram(programme);
                        items.add(programme);
                    }

                    break;
            }
        }

        return schedule;
    }

    public Programme parseProgramme(Node programmNode)
    {
        String programmeId = MetadataUtils.computeId(getNodeAttr("id", programmNode));
        if(programmeId.isEmpty())
            return null;

        Programme programme = new Programme(programmeId);

        NodeList programmeChildNodes = programmNode.getChildNodes();

        for (int i = 0; i < programmeChildNodes.getLength(); i++)
        {
            Node programmeNode = programmeChildNodes.item(i);

            switch (programmeNode.getNodeName().toLowerCase())
            {
                case "longname":
                    programme.setLongName(getNodeValue(programmeNode));
                    break;
                case "mediumname":
                    programme.setName(getNodeValue(programmeNode));
                    break;

                case "mediadescription":
                    NodeList mediaDescNodes = programmeNode.getChildNodes();

                    for (int i2 = 0; i2 < mediaDescNodes.getLength(); i2++)
                    {
                        Node descNode = mediaDescNodes.item(i2);

                        if (descNode.getNodeName().equalsIgnoreCase("longdescription"))
                        {
                            programme.setDescription(getNodeValue(descNode));
                            break;
                        }
                    }

                    break;

                case "genre":
                    programme.addGenre(new Genre(getNodeValue(programmeNode)));
                    break;

                case "location":
                    Node timeNode = getNode("time", programmeNode.getChildNodes());
                    if (timeNode != null)
                    {
                        String strTime = getNodeAttr("time", timeNode);
                        String strDuration = getNodeAttr("duration", timeNode);

                        ReadableDuration duration = ISOPeriodFormat.standard().parsePeriod(strDuration).toStandardDuration();

                        DateTime startTime = new DateTime(CommonUtils.toDate(strTime.split("\\+")[0], "yyyy-MM-dd'T'HH:mm:ss"));

                        DateTime stopTime = startTime.withDurationAdded(duration, 1);

                        programme.setStartTime(startTime);
                        programme.setStopTime(stopTime);

                        break;
                    }
            }
        }

        return programme;
    }


    private Node getNode(String tagName, NodeList nodes)
    {
        for (int x = 0; x < nodes.getLength(); x++)
        {
            Node node = nodes.item(x);
            if (node.getNodeName().equalsIgnoreCase(tagName))
            {
                return node;
            }
        }

        return null;
    }

    private String getNodeValue(Node node)
    {
        NodeList childNodes = node.getChildNodes();
        for (int x = 0; x < childNodes.getLength(); x++)
        {
            Node data = childNodes.item(x);
            if (data.getNodeType() == Node.TEXT_NODE || data.getNodeType() == Node.CDATA_SECTION_NODE) {
                String value = data.getNodeValue().trim();
                if(!value.equalsIgnoreCase("") && !onlyConsistsOfSpecialChars(value)) return value;
            }

        }
        return "NO_VALID_VALUE_FOUND: " + node.toString();
    }

    private String getNodeValue(String tagName, NodeList nodes)
    {
        for (int x = 0; x < nodes.getLength(); x++)
        {
            Node node = nodes.item(x);
            if (node.getNodeName().equalsIgnoreCase(tagName))
            {
                return getNodeValue(node);
            }
        }
        return "";
    }

    private String getNodeAttr(String attrName, Node node)
    {
        NamedNodeMap attrs = node.getAttributes();
        for (int y = 0; y < attrs.getLength(); y++)
        {
            Node attr = attrs.item(y);
            if (attr.getNodeName().equalsIgnoreCase(attrName))
            {
                return attr.getNodeValue();
            }
        }
        return "";
    }

    private String getNodeAttr(String tagName, String attrName, NodeList nodes)
    {
        for (int x = 0; x < nodes.getLength(); x++)
        {
            Node node = nodes.item(x);
            if (node.getNodeName().equalsIgnoreCase(tagName))
            {
                NodeList childNodes = node.getChildNodes();
                for (int y = 0; y < childNodes.getLength(); y++)
                {
                    Node data = childNodes.item(y);
                    if (data.getNodeType() == Node.ATTRIBUTE_NODE)
                    {
                        if (data.getNodeName().equalsIgnoreCase(attrName))
                            return data.getNodeValue();
                    }
                }
            }
        }

        return "";
    }
}
