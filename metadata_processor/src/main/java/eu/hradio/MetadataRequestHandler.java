package eu.hradio;


import com.rabbitmq.client.*;
import eu.hradio.RequestHandler.*;
import eu.hradio.commons.*;
import eu.hradio.metadata.search.MetadataRequest;
import eu.hradio.metadata.search.MetadataRequestType;
import eu.hradio.metadata.search.MetadataResponse;
import eu.hradio.metadata.search.MetadataResponseType;
import eu.hradio.metadata.storage.IStorageClient;
import eu.hradio.metadata.*;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;


/***
 * Consumes RabbitMQ channel and handles metadata requests
 */
public class MetadataRequestHandler
{
    static org.apache.logging.log4j.Logger _log = LogManager.getLogger(MetadataRequestHandler.class);

    public MetadataRequestHandler(RMQConnection rmqConnection, IStorageClient storageClient, String rmqRequestQueue) throws IOException
    {
        _rmqConnection = rmqConnection;
        _rmqRequestQueue = rmqRequestQueue;

        _storageClient = storageClient;

        _rmqChannel = _rmqConnection.createChannel();
        _rmqChannel.queueDeclare(_rmqRequestQueue, false, false, false, null);
        _rmqChannel.basicQos(1);

        _handlerMap = new HashMap<>();

        _handlerMap.put(new Pair(MetadataType.SERVICE_PROVIDER, MetadataRequestType.GET), new GetHandler());
        _handlerMap.put(new Pair(MetadataType.SERVICE, MetadataRequestType.GET), new GetHandler());
        _handlerMap.put(new Pair(MetadataType.SCHEDULE, MetadataRequestType.GET), new GetHandler());
        _handlerMap.put(new Pair(MetadataType.PROGRAMME, MetadataRequestType.GET), new GetHandler());

        _handlerMap.put(new Pair(MetadataType.SERVICE_PROVIDER, MetadataRequestType.DELETE), new DeleteHandler());
        _handlerMap.put(new Pair(MetadataType.SERVICE, MetadataRequestType.DELETE), new DeleteHandler());
        _handlerMap.put(new Pair(MetadataType.SCHEDULE, MetadataRequestType.DELETE), new DeleteHandler());
        _handlerMap.put(new Pair(MetadataType.PROGRAMME, MetadataRequestType.DELETE), new DeleteHandler());

        _handlerMap.put(new Pair(MetadataType.SERVICE_PROVIDER, MetadataRequestType.CREATE), new CreateHandler());
        _handlerMap.put(new Pair(MetadataType.SERVICE, MetadataRequestType.CREATE), new CreateHandler());
        _handlerMap.put(new Pair(MetadataType.SCHEDULE, MetadataRequestType.CREATE), new CreateHandler());
        _handlerMap.put(new Pair(MetadataType.PROGRAMME, MetadataRequestType.CREATE), new CreateHandler());

        Consumer consumer = new DefaultConsumer(_rmqChannel)
        {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException
            {
                AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(properties.getCorrelationId())
                        .build();

                MetadataResponse resp = null;

                try
                {
                    MetadataRequest request = MetadataUtils.gson.fromJson(new String(body,"UTF-8"), MetadataRequest.class);

                    IRequestHandler handler = _handlerMap.get(new Pair(request.getMetadataType(), request.getRequestType()));
                    if(handler == null)
                        throw new Exception("Couldn't find request handler.");

                    resp = handler.handle(request, storageClient);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    _log.error(e.getMessage());
                    resp = new MetadataResponse(e.getMessage(), MetadataResponseType.ERROR);
                }
                finally
                {
                    _rmqChannel.basicPublish( "", properties.getReplyTo(), replyProps, MetadataUtils.gson.toJson(resp).getBytes("UTF-8"));
                    _rmqChannel.basicAck(envelope.getDeliveryTag(), false);
                }
            }
        };

        _rmqChannel.basicConsume(_rmqRequestQueue, false, consumer);
    }

    public void close() throws IOException, TimeoutException
    {
        if(_rmqChannel.isOpen())
            _rmqChannel.close();
    }

    private String _rmqRequestQueue;
    private RMQConnection _rmqConnection;
    private Channel _rmqChannel;

    private IStorageClient _storageClient;

    private HashMap<Pair<MetadataType, MetadataRequestType>, IRequestHandler> _handlerMap;
}
