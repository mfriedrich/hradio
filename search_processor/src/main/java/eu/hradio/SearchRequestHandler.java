package eu.hradio;


import com.rabbitmq.client.*;
import eu.hradio.commons.Pair;
import eu.hradio.commons.RMQConnection;
import eu.hradio.metadata.MetadataUtils;
import eu.hradio.metadata.search.*;
import org.apache.logging.log4j.LogManager;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.rest.RestStatus;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;


/***
 * Handles search requests, consumes RabbitMQ channel and delivers requests
 * to corresponding request processor
 */
public class SearchRequestHandler
{
    static org.apache.logging.log4j.Logger _log = LogManager.getLogger(SearchRequestHandler.class);

    public SearchRequestHandler(RMQConnection rmqConnection, String rmqRequestQueue, HashMap<Pair<SearchFederationMode, SearchRequestQuerySyntax>, ISearchProcessor> searchProcessorMap) throws IOException, TimeoutException
    {
        _rmqConnection = rmqConnection;
        _rmqRequestQueue = rmqRequestQueue;

        _searchProcessorMap = searchProcessorMap;

        _rmqChannel = _rmqConnection.createChannel();
        _rmqChannel.queueDeclare(_rmqRequestQueue, false, false, false, null);
        _rmqChannel.basicQos(1);

        Consumer consumer = new DefaultConsumer(_rmqChannel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException
            {
                AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(properties.getCorrelationId())
                        .build();

                MetadataResponse resp = null;

                try
                {
                    _log.info(String.format("Received message: %s", new String(body,"UTF-8")));

                    eu.hradio.metadata.search.SearchRequest request = MetadataUtils.gson.fromJson(new String(body,"UTF-8"), eu.hradio.metadata.search.SearchRequest.class);

                    ISearchProcessor processor = searchProcessorMap.get(new Pair<>(request.getSearchFederationMode(), request.getQuerySyntax()));
                    if(processor == null)
                        throw new Exception(String.format("No search processor exists for federation mode %s and search engine %s.",
                                request.getSearchFederationMode().toString(), request.getQuerySyntax()));

                    resp =  processor.search(request);
                }
                catch (Exception e)
                {
                    _log.error(e.toString());
                    resp = new MetadataResponse(e.getMessage(), MetadataResponseType.ERROR);
                }
                finally
                {
                    _rmqChannel.basicPublish( "", properties.getReplyTo(), replyProps, MetadataUtils.gson.toJson(resp).toString().getBytes("UTF-8"));
                    _rmqChannel.basicAck(envelope.getDeliveryTag(), false);
                }
            }
        };

        _rmqChannel.basicConsume(_rmqRequestQueue, false, consumer);
    }

    public MetadataResponse checkMetadataResponse(SearchResponse response)
    {
        if (response.status() != RestStatus.OK)
            return new MetadataResponse(ErrorMessages.elasticSearchError, MetadataResponseType.ERROR);

        if (response.isTimedOut())
            return new MetadataResponse(ErrorMessages.elasticSearchTimeout, MetadataResponseType.TIMEOUT);

        if(response.getHits().totalHits == 0)
            return new MetadataResponse(ErrorMessages.elasticSearchNotFound, MetadataResponseType.NOT_FOUND);

        return new MetadataResponse("", MetadataResponseType.OK);
    }

    public void close() throws IOException, TimeoutException
    {
        if(_rmqChannel.isOpen())
            _rmqChannel.close();
    }

    private String _rmqRequestQueue;
    private RMQConnection _rmqConnection;
    private Channel _rmqChannel;

    private HashMap<Pair<SearchFederationMode, SearchRequestQuerySyntax>, ISearchProcessor> _searchProcessorMap;
}
