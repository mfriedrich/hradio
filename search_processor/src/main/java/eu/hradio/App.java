package eu.hradio;

import eu.hradio.commons.Pair;
import eu.hradio.commons.RMQConfig;
import eu.hradio.commons.RMQConnection;

import eu.hradio.metadata.search.*;
import eu.hradio.metadata.storage.IStorageClient;
import eu.hradio.metadata.storage.elastic.ESConfig;
import eu.hradio.metadata.storage.elastic.ESStorageClient;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.util.HashMap;

import static eu.hradio.metadata.Constants.programmeQueryQueue;
import static eu.hradio.metadata.Constants.scheduleQueryQueue;
import static eu.hradio.metadata.Constants.serviceQueryQueue;


public class App implements CommandLineRunner
{
    static org.apache.logging.log4j.Logger _log = LogManager.getLogger(App.class);

    @Autowired
    private ConfigurableApplicationContext context;

    @Value("${rabbitmq.address}")
    String rmqAddress;

    @Value("${rabbitmq.port}")
    int rmqPort;

    @Value("${rabbitmq.user}")
    String rmqUser;

    @Value("${rabbitmq.password}")
    String rmqPassword;

    @Value("${rabbitmq.retries}")
    int rmqRetries;

    @Value("${rabbitmq.retryInterval}")
    int rmqRetryInterval;

    @Value("${elastic.protocol}")
    String esProtocol;

    @Value("${elastic.address}")
    String esAddress;

    @Value("${elastic.port}")
    int esPort;

    @Value("${elastic.requestTimeout}")
    int esRequestTimeout;

    @Value("${elastic.retries}")
    int esRetries;

    @Value("${elastic.retryInterval}")
    int esRetryInterval;

    @Value("${elastic.deleteIndicesIfExisting}")
    boolean esDeleteIndicesIfExisting;


    public static void main(String[] args)
    {
        SpringApplication app = new SpringApplication(App.class);
        //app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception
    {
        try
        {
            _log.info("Setup Elastic Search connection...");
            ESConfig elasticConfig = new ESConfig(esProtocol, esAddress, esPort, esRequestTimeout, esRetries, esRetryInterval, esDeleteIndicesIfExisting);
            IStorageClient storageClient = createStorageClient(elasticConfig,SearchEngine.ELASTIC);
            _log.info("Done.");

            HashMap<Pair<SearchFederationMode, SearchRequestQuerySyntax>, ISearchProcessor> serviceSearchProcessors = new HashMap<Pair<SearchFederationMode, SearchRequestQuerySyntax>, ISearchProcessor>();
            serviceSearchProcessors.put(new Pair<>(SearchFederationMode.LOCAL, SearchRequestQuerySyntax.ELASTIC_COMPLEX),
                    createSearchProcessor(storageClient, SearchFederationMode.LOCAL));
            serviceSearchProcessors.put(new Pair<>(SearchFederationMode.LOCAL, SearchRequestQuerySyntax.ELASTIC_QUERY_STRING),
                    createSearchProcessor(storageClient, SearchFederationMode.LOCAL));

            HashMap<Pair<SearchFederationMode, SearchRequestQuerySyntax>, ISearchProcessor> scheduleSearchProcessors = new HashMap<Pair<SearchFederationMode, SearchRequestQuerySyntax>, ISearchProcessor>();
            scheduleSearchProcessors.put(new Pair<>(SearchFederationMode.LOCAL, SearchRequestQuerySyntax.ELASTIC_COMPLEX),
                    createSearchProcessor(storageClient, SearchFederationMode.LOCAL));
            scheduleSearchProcessors.put(new Pair<>(SearchFederationMode.LOCAL, SearchRequestQuerySyntax.ELASTIC_QUERY_STRING),
                    createSearchProcessor(storageClient, SearchFederationMode.LOCAL));

            HashMap<Pair<SearchFederationMode, SearchRequestQuerySyntax>, ISearchProcessor> programmeSearchProcessors = new HashMap<Pair<SearchFederationMode, SearchRequestQuerySyntax>, ISearchProcessor>();
            programmeSearchProcessors.put(new Pair<>(SearchFederationMode.LOCAL, SearchRequestQuerySyntax.ELASTIC_COMPLEX),
                    createSearchProcessor(storageClient, SearchFederationMode.LOCAL));
            programmeSearchProcessors.put(new Pair<>(SearchFederationMode.LOCAL, SearchRequestQuerySyntax.ELASTIC_QUERY_STRING),
                    createSearchProcessor(storageClient, SearchFederationMode.LOCAL));

            _log.info("Setup RabbitMQ connection...");
            RMQConnection rmqCon = new RMQConnection(new RMQConfig(rmqAddress, rmqUser, rmqPassword, rmqPort, rmqRetries, rmqRetryInterval));
            _log.info("Done.");

            SearchRequestHandler serviceQRH = new SearchRequestHandler(rmqCon, serviceQueryQueue, serviceSearchProcessors);
            SearchRequestHandler scheduleQRH = new SearchRequestHandler(rmqCon, scheduleQueryQueue, scheduleSearchProcessors);
            SearchRequestHandler programmeQRH = new SearchRequestHandler(rmqCon, programmeQueryQueue, programmeSearchProcessors );

            Runtime.getRuntime().addShutdownHook(new Thread()
            {
                public void run()
                {
                    try
                    {
                        serviceQRH.close();
                        scheduleQRH.close();
                        programmeQRH.close();
                        rmqCon.close();

                        _log.info("User-triggered shutdown.");

                    } catch (Exception ex)
                    {
                        _log.error(ex.getMessage());
                    }
                }
            });

        } catch (Exception ex)
        {
            _log.fatal(ex.getMessage());
            exitWithError();
        }
    }

    private IStorageClient createStorageClient(ESConfig config, SearchEngine engine) throws IOException
    {
        IStorageClient sClient = null;
        switch(engine)
        {
            case ELASTIC:
            case DEFAULT:
            default:
                sClient = new ESStorageClient(config, false);
                break;
        }
        return sClient;
    }

    private ISearchProcessor createSearchProcessor(IStorageClient client, SearchFederationMode fedMode) throws Exception
    {
        ISearchProcessor processor = null;
        switch(fedMode)
        {
            case FEDERATED:
                throw new Exception("Federated search mode is currently not supported.");
            case LOCAL:
            default:
                processor = new LocalSearchProcessor(client);
        }

        return processor;
    }

    private void exitWithError()
    {
        context.close();
        System.exit(1);
    }
}
