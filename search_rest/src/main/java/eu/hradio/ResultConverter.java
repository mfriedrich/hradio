package eu.hradio;

import eu.hradio.metadata.search.MetadataResponse;
import eu.hradio.metadata.search.MetadataResponseType;
import eu.hradio.metadata.search.SearchRequest;
import eu.hradio.metadata.search.SearchRequester;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


/***
 * Converts responses to metadata requests to HTTP responses
 */
public class ResultConverter
{
    public static ResponseEntity<?> request(SearchRequester requester, SearchRequest request)
    {
        try
        {
            MetadataResponse resp = requester.request(request);

            return new ResponseEntity(resp.getContent(), toStatusCode(resp.getType()));
        }
        catch(Exception ex)
        {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private static HttpStatus toStatusCode(MetadataResponseType type)
    {
        switch(type)
        {
            case OK:
                return HttpStatus.OK;
            case TIMEOUT:
                return HttpStatus.REQUEST_TIMEOUT;
            case NOT_FOUND:
                return HttpStatus.NOT_FOUND;
            case ERROR:
                return HttpStatus.BAD_REQUEST;
        }

        return HttpStatus.BAD_REQUEST;
    }
}
