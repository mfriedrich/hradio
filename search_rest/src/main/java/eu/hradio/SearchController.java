package eu.hradio;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import eu.hradio.commons.*;

import eu.hradio.metadata.MetadataType;
import eu.hradio.metadata.search.SearchRequest;
import eu.hradio.metadata.search.SearchRequestQuerySyntax;
import eu.hradio.metadata.search.SearchRequester;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/***
 * Controls and handles metadata search requests via a RESTful search interface
 * and provides request mappings for HTTP requests
 */
@RestController
public class SearchController
{
    static org.apache.logging.log4j.Logger _log = LogManager.getLogger(SearchController.class);

    public SearchController(@Value("${rabbitmq.address}") String rmqAddress, @Value("${rabbitmq.port}") int rmqPort,
                            @Value("${rabbitmq.user}") String rmqUser, @Value("${rabbitmq.password}") String rmqPassword,
                            @Value("${rabbitmq.retries}") int retries, @Value("${rabbitmq.retryInterval}") int retryInterval,
                            @Value("${rest.requestTimeout}") int requestTimeout) throws IOException, TimeoutException, InterruptedException
    {
        RMQConnection rmqCon = new RMQConnection(new RMQConfig(rmqAddress, rmqUser, rmqPassword, rmqPort, retries, retryInterval));

        _serviceQueryReq = SearchRequester.createServiceSearchRequester(rmqCon, requestTimeout);
        _scheduleQueryReq = SearchRequester.createScheduleSearchRequester(rmqCon, requestTimeout);
        _programmeQueryReq = SearchRequester.createProgrammeSearchRequester(rmqCon, requestTimeout);

        _log.info("Starting Search REST search interface...");
    }

    @RequestMapping(value="/services", method=RequestMethod.POST)
    public ResponseEntity<?> requestServicesViaBodyQuery(@RequestBody(required=true) String bodyQuery,
                                                         @RequestParam(value="size", defaultValue="10") int size,
                                                         @RequestParam(value="from", defaultValue="0") int from,
                                                         @RequestParam(value="maxhops", defaultValue="0") int maxSearchHops,
                                                         @RequestParam(value="blacklist", defaultValue="") String searchHostBlacklist,
                                                         @RequestParam(value="whitelist", defaultValue="") String searchHostWhitelist
    ) throws IOException, InterruptedException
    {
        return eu.hradio.ResultConverter.request(_serviceQueryReq,
                new SearchRequest(MetadataType.SERVICE, bodyQuery, size, from, SearchRequestQuerySyntax.ELASTIC_COMPLEX, maxSearchHops, searchHostBlacklist, searchHostWhitelist));
    }

    @RequestMapping(value="/services", method=RequestMethod.GET)
    public ResponseEntity<?> requestServicesViaQueryString(@RequestParam(value="q", defaultValue="") String query,
                                                           @RequestParam(value="size", defaultValue="10") int size,
                                                           @RequestParam(value="from", defaultValue="0") int from,
                                                           @RequestParam(value="maxhops", defaultValue="0") int maxSearchHops,
                                                           @RequestParam(value="blacklist", defaultValue="") String searchHostBlacklist,
                                                           @RequestParam(value="whitelist", defaultValue="") String searchHostWhitelist
    ) throws IOException, InterruptedException
    {
        return eu.hradio.ResultConverter.request(_serviceQueryReq,
                new SearchRequest(MetadataType.SERVICE, query, size, from, SearchRequestQuerySyntax.ELASTIC_QUERY_STRING, maxSearchHops, searchHostBlacklist, searchHostWhitelist));
    }

    @RequestMapping(value="/schedules", method=RequestMethod.POST)
    public ResponseEntity<?> requestSchedulesViaBodyQuery(@RequestBody(required=true) String bodyQuery,
                                                         @RequestParam(value="size", defaultValue="10") int size,
                                                         @RequestParam(value="from", defaultValue="0") int from,
                                                         @RequestParam(value="maxhops", defaultValue="0") int maxSearchHops,
                                                         @RequestParam(value="blacklist", defaultValue="") String searchHostBlacklist,
                                                         @RequestParam(value="whitelist", defaultValue="") String searchHostWhitelist
    ) throws IOException, InterruptedException
    {
        return eu.hradio.ResultConverter.request(_scheduleQueryReq,
                new SearchRequest(MetadataType.SCHEDULE, bodyQuery, size, from, SearchRequestQuerySyntax.ELASTIC_COMPLEX, maxSearchHops, searchHostBlacklist, searchHostWhitelist));
    }

    @RequestMapping(value="/schedules", method=RequestMethod.GET)
    public ResponseEntity<?> requestSchedulesViaQueryString(@RequestParam(value="q", defaultValue="") String query,
                                                           @RequestParam(value="size", defaultValue="10") int size,
                                                           @RequestParam(value="from", defaultValue="0") int from,
                                                           @RequestParam(value="maxhops", defaultValue="0") int maxSearchHops,
                                                           @RequestParam(value="blacklist", defaultValue="") String searchHostBlacklist,
                                                           @RequestParam(value="whitelist", defaultValue="") String searchHostWhitelist
    ) throws IOException, InterruptedException
    {
        return eu.hradio.ResultConverter.request(_scheduleQueryReq,
                new SearchRequest(MetadataType.SCHEDULE, query, size, from, SearchRequestQuerySyntax.ELASTIC_QUERY_STRING, maxSearchHops, searchHostBlacklist, searchHostWhitelist));
    }

    @RequestMapping(value="/programmes", method=RequestMethod.POST)
    public ResponseEntity<?> requestProgrammesViaBodyQuery(@RequestBody(required=true) String bodyQuery,
                                                          @RequestParam(value="size", defaultValue="10") int size,
                                                          @RequestParam(value="from", defaultValue="0") int from,
                                                          @RequestParam(value="maxhops", defaultValue="0") int maxSearchHops,
                                                          @RequestParam(value="blacklist", defaultValue="") String searchHostBlacklist,
                                                          @RequestParam(value="whitelist", defaultValue="") String searchHostWhitelist
    ) throws IOException, InterruptedException
    {
        return eu.hradio.ResultConverter.request(_programmeQueryReq,
                new SearchRequest(MetadataType.PROGRAMME, bodyQuery, size, from, SearchRequestQuerySyntax.ELASTIC_COMPLEX, maxSearchHops, searchHostBlacklist, searchHostWhitelist));
    }

    @RequestMapping(value="/programmes", method=RequestMethod.GET)
    public ResponseEntity<?> requestProgrammesViaQueryString(@RequestParam(value="q", defaultValue="") String query,
                                                            @RequestParam(value="size", defaultValue="10") int size,
                                                            @RequestParam(value="from", defaultValue="0") int from,
                                                            @RequestParam(value="maxhops", defaultValue="0") int maxSearchHops,
                                                            @RequestParam(value="blacklist", defaultValue="") String searchHostBlacklist,
                                                            @RequestParam(value="whitelist", defaultValue="") String searchHostWhitelist
    ) throws IOException, InterruptedException
    {
        return eu.hradio.ResultConverter.request(_programmeQueryReq,
                new SearchRequest(MetadataType.PROGRAMME, query, size, from, SearchRequestQuerySyntax.ELASTIC_QUERY_STRING, maxSearchHops, searchHostBlacklist, searchHostWhitelist));
    }

    private SearchRequester _serviceQueryReq;
    private SearchRequester _scheduleQueryReq;
    private SearchRequester _programmeQueryReq;
}